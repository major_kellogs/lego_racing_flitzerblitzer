#!/bin/bash
set -e # exit if any error occurs

# build project from source, without eclipse
BUILDDIR="/tmp/LegoRacing_build"
LNAME="LegoRacing"

if [ -d "$BUILDDIR" ] ; then
    echo "$BUILDDIR exists. Cleaning up."
    rm -r "$BUILDDIR"
fi

mkdir -p "$BUILDDIR"
javac -d "$BUILDDIR" -classpath "libs/*:libs/usb4java-1.2.0/lib/*:libs/usb4java-javax-1.2.0/lib/*" src/*/*.java

# create JAR, remove classfiles
OWD="$(pwd)"
cd "$BUILDDIR"
jar cf "$LNAME.jar" *
rm -r backend application
cd "$OWD"

# copy resources
cp -r resources "$BUILDDIR"
rm -f "$BUILDDIR/resources/"*-backup1.wav # d'oh
mkdir "$BUILDDIR/resources/application"
cp src/application/*.fxml "$BUILDDIR/resources/application"
cp -r src/images "$BUILDDIR/resources"
cp -r src/themes "$BUILDDIR/resources"

mkdir -p "$BUILDDIR/libs"
# copy all libs (flat, not retaining subdirs for a shorter classpath in launch-script)
# also exclude RXTX (we use native one from the system) and usb4java native-libs for win & osx
find libs -name '*.jar' \
    -and -not -name "RXTXcomm.jar" \
    -and -not -name "libusb4java-*-osx-*.jar" \
    -and -not -name "libusb4java-*-windows-*.jar" \
    -exec cp '{}' "$BUILDDIR/libs" \;


cat >"$BUILDDIR/launch.sh" <<-LAUNCHSCRIPT
	#!/bin/bash
	
	BIN="\$(dirname "\$(readlink "\$0")")"
	JRXTX="/usr/share/java/RXTXcomm.jar"
	if [ ! -e "\$JRXTX" ] ; then
	    JRXTX="/usr/share/java/rxtx/RXTXcomm.jar"
	fi
	# ...
	if [ ! -e "\$JRXTX" ] ; then
	    echo "java rxtx library not found." >&2
	    exit 1
	fi
	
	# java 8!
	JAV="\$(ls /usr/lib/jvm/java-8-open*/jre/bin/java)"
	if [ ! -x "\$JAV" ] ; then
	    echo "OpenJDK 8 not found." >&2
	    exit 1
	fi
	
	cd "\$BIN"
	\$JAV -Dfile.encoding=UTF-8 -classpath "\$JRXTX:libs/*:resources:LegoRacing.jar" application.Main >/dev/null
LAUNCHSCRIPT
chmod a+x "$BUILDDIR/launch.sh"

echo "Deploy DONE. See $BUILDDIR. Let's create a DEB."


# prepare DEB package (for Ubuntu 18.04 and probably other)
PKGDIR="/tmp/LegoRacing_pkg/"
if [ -d "$PKGDIR" ] ; then
    echo "$PKGDIR exists. Cleaning up."
    rm -r "$PKGDIR"
fi

# copy everything
mkdir -p "$PKGDIR/usr/lib/$LNAME"
cp -r "$BUILDDIR"/* "$PKGDIR/usr/lib/$LNAME"
mkdir -p "$PKGDIR/usr/share/pixmaps"
cp "src/images/logo.png" "$PKGDIR/usr/share/pixmaps/$LNAME.png"

# create launcher
mkdir -p "$PKGDIR/usr/share/applications/"
cat >"$PKGDIR/usr/share/applications/$LNAME.desktop" <<=====
[Desktop Entry]
Name=HOST Lego Racing
GenericName=HOST Lego Racing
Exec=LegoRacing
Icon=LegoRacing
Categories=Education;Science
Comment=Software zur Zeiterfassung der Lego-Erstsemester-Challenge an der Hochschule Stralsund
Terminal=false
Type=Application
StartupNotify=true
=====

# create udev-rules
mkdir -p "$PKGDIR/etc/udev/rules.d"
cat >"$PKGDIR/etc/udev/rules.d/99-legoracing.rules" <<=====
ACTION=="add", ATTRS{idProduct}=="7523", ATTRS{idVendor}=="1a86", MODE="0666"
ACTION=="add", ATTRS{idProduct}=="ea60", ATTRS{idVendor}=="10c4", MODE="0666"
=====

# Package meta-data
# These are the mandatory fields in the control file. For more options see:
# http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-binarycontrolfiles
mkdir -p "$PKGDIR/DEBIAN"
cat >"$PKGDIR/DEBIAN/control" <<=====
Package: LegoRacing
Version: 0.8b
Maintainer: Robert Pfeiffer
Architecture: all
Homepage: https://bitbucket.org/major_kellogs/lego_racing_flitzerblitzer
Depends: openjfx, librxtx-java
Description: Eine Software zur Zeiterfassung der Lego-Erstsemester-Challenge
=====

cat >"$PKGDIR/DEBIAN/postinst" <<=====
#!/bin/bash
udevadm control --reload
ln -s "/usr/lib/$LNAME/launch.sh" "/usr/bin/$LNAME"
=====
chmod a+x "$PKGDIR/DEBIAN/postinst"

cat >"$PKGDIR/DEBIAN/prerm" <<=====
#!/bin/bash
rm "/usr/bin/$LNAME"
=====
chmod a+x "$PKGDIR/DEBIAN/prerm"

echo "Tree $PKGDIR created, checking for dpkg-deb"
if ! which dpkg-deb 2>/dev/null ; then
    echo "dpkg-deb not found. Create .deb elsewhere:"
    echo "$ dpkg-deb --root-owner-group -v --build $PKGDIR LegoRacing.deb"
    exit 1
fi

# check version
DPKG="$(LC_ALL=C dpkg-deb --version | head -1 | sed -r 's/.*version ([0-9]+\.[0-9]+)\..*/\1/')"
echo "Found Version $DPKG"
if [ $(bc <<< "$DPKG >= 1.19") -eq 1 ] ; then
    dpkg-deb --root-owner-group --build "$PKGDIR" LegoRacing.deb
else
    echo "Older verion doen't support --root-owner-group, so reown $PKGDIR to root"
    sudo chown -R root:root "$PKGDIR"
    dpkg-deb --build "$PKGDIR" LegoRacing.deb
    sudo chown -R "$USER:$(id -g)" "$PKGDIR"
fi
echo "Made a pakage: $(du -h LegoRacing.deb)"

# INSTALL:
#~ sudo apt-add-repository -u universe
#~ sudo apt-get install ./LegoRacing.deb

# zum komplett lokalen bauen (in der VM z.B.) braucht man noch openjdk-8-jdk
