/**
 * @mainpage LegoRacing - FlitzerBlitzer 
 *
 *
 * 
# Inhalt #
  -  <a href="#Quick">Quick Start Guide</a>
    -  <a href="#Installation">Installation</a>
    -  <a href="#Softwarevorraussetzungen">Softwarevorraussetzungen</a>
    -  <a href="#Wesentlicher">Wesentlicher Funktionsumfang</a>
      -  <a href="#Teams">Teams</a>
        -  <a href="#erstellen">Team erstellen</a>
        -  <a href="#bearbeiten">Team bearbeiten</a>
        -  <a href="#löschen">Team löschen</a>
      -  <a href="#durchführen">Neues Rennen durchführen</a>
        -  <a href="#Vorgehensweise">Vorgehensweise</a>
      -  <a href="#Erfolgte">Erfolgte Rennen  - Detailansicht</a>
        -  <a href="#exportieren">Rennen exportieren</a>
        -  <a href="#Siegerurkunden">Siegerurkunden erstellen</a>
        -  <a href="#rlöschen">Rennen löschen</a>
      -  <a href="#importieren">Datenset importieren</a>
      -  <a href="#dexportieren">Datenset exportieren</a>
      -  <a href="#Einstellungen">Einstellungen ändern</a>
      
  -  <a href="#Developers">Developers Guide</a>
    -  <a href="#Programmstruktur">Programmstruktur</a>
    -  <a href="#Entwicklungsumgebungen">Verwendete Entwicklungsumgebungen</a>
      -  <a href="#LegoRacing_ClientLegoRacing_Client">LegoRacing_Client</a>
        -  <a href="#Plugins">verwendete Plugins</a>
      -  <a href="#LegoRacing_Hardwaresteuerung">LegoRacing_Hardwaresteuerung</a>
    -  <a href="#Persistenz">Persistenz und Speicherorte(nach Installation)</a>
      -  <a href="#Datenbank">Datenbank</a>
      -  <a href="#Programmeinstellungen">Programmeinstellungen</a>
      -  <a href="#Programmbibliotheken">Programmbibliotheken</a>
      -  <a href="#Icon">Icon</a>
      -  <a href="#Menü">Menü-Config</a>
      -  <a href="#Programmdateien">Programmdateien</a>
    -  <a href="#Hardwaremodifikation">Hardwaremodifikation</a>



# <a name="Quick">Quick Start Guide</a> #



Diese Software wurde für die Verwendung unter Linux entwickelt. Die Lauffähigkeit wird unter Ubuntu 16.04 - Ubuntu 18.04 gewährleistet. Prinzipiell sollte eine Verwendung aber unter allen aktuellen (Stand: 2018-08-31), Debian-basierenden Desktopbetriebssystemen möglich sein.

Die Software wurde getestet unter:

  -  Ubuntu 16.04 (Native Installation)
  -  Xubuntu 18.04 (LiveUSB)
  
Empfohlen wird die Verwendung unter Ubuntu 18.04, dieses kann kostenfrei unter <a href="https://www.ubuntu.com/download/desktop" target="_blank">https://www.ubuntu.com/download/desktop</a> (Stand: 2018-08-31) heruntergeladen werden.

## <a name="Installation">Installation</a> ##

Zur Installation sind Administratorrechte erforderlich.

1. Öfnnen Sie ein Ternimal und navigieren Sie in das Verzeichnis mit der Datei "LegoRacing.deb" (Hilfe unter: <a href="https://wiki.ubuntuusers.de/Terminal/" target="_blank">https://wiki.ubuntuusers.de/Terminal/</a> (Stand: 2018-08-31))

2. Überprüfen Sie mit folgendem Befehl ob das Repository "universe" in den Paketquelle des Systems eingebunden ist und folgen Sie ggf. den Anweisungen.:

	1. Unter Ubunu 16.04
	
			sudo apt-add-repository universe		
			sudo apt-get update

	2. Unter Ubuntu 18.04
	
			sudo apt-add-repository -u universe
		
3. Installieren Sie die Software mit folgendem Befehl, oder nutzen Sie die Paketverwaltung Ihres Betriebssystems z.B.: Ubunu-Software-Center
		
		sudo apt-get install ./LegoRacing.deb
		
4. Die Installation ist nun abgeschlossen. Sie können die Software über die Menüstruktur Ihres Windowmanagers starten, oder folgenden Befehl nutzen:

		LegoRacing

## <a name="Softwarevorraussetzungen">Softwarevorraussetzungen</a> ##

Alle Softwarevorraussetzungen werden mit der Installation erfüllt.
Erforderlich sind die Pakete:

  -  openjdk-8-jre 
	  
  -  librxtx-java

## <a name="Wesentlicher">Wesentlicher Funktionsumfang</a> ##

### <a name="Teams">Teams</a> ###

#### <a name="erstellen">Team erstellen</a> ####
1. Wählen Sie den Reiter "Teamverwaltung" oder "Neues Rennen" aus.
2. Klicken Sie auf den Button "Ein neues Team erstellen", dieser ist mit einem grünen Plus gekennzeichnet. 
3. Füllen Sie alle Felder der Maske aus (Teammitglieder werden Zeilengetrennt).
4. Klicken Sie auf den Button "Team speichern".

#### <a name="bearbeiten">Team bearbeiten</a> ####
1. Wählen Sie den Reiter "Teamverwaltung" aus.
2. Wählen Sie ein Team aus der Liste aus.
3. Klicken Sie auf den Button "Das ausgewählte Team bearbeiten", dieser ist mit einem blauen Stift gekennzeichnet. 
4. Nehmen Sie die Änderungen vor.
5. Klicken Sie auf den Button "Team speichern".

#### <a name="löschen">Team löschen</a> ####
1. Wählen Sie den Reiter "Teamverwaltung" aus.
2. Wählen Sie ein Team aus der Liste aus.
3. Klicken Sie auf den Button "Das ausgewählte Team dauerhaft löschen", dieser ist mit einem roten X gekennzeichnet. 

### <a name="durchführen">Neues Rennen durchführen</a> ###

Um ein neues Rennen zu starten:

  -  muss die Stoppuhr "ELV LSU 100" mit dem Computer verbunden und von der Software korrekt erkannt worden sein. Dies erkennen Sie in der Statusleiste am unteren Fensterrand.
  -  muss die Stoppuhr korrekt konfiguriert sein, damit Sie über Lichtschranken die Messunf auslösen können. Dies erreichen Sie über den "Mode"-Knopf an der Stoppuhr. Als Orientierung für den korrekten Modus dient Ihnen die Tafel an der Unterseite der Stoppuhr.
  -  müssen mindestens zwei Teams angelegt sein.

#### <a name="Vorgehensweise">Vorgehensweise</a> ####
1. Wählen Sie den reiter "Neues Rennen"
2. Geben Sie in dem Feld "Name des Rennes" den Namen des Rennens ein.
3. Wählen aus der Liste mindestens zwei Teams aus, die an dem Rennen teilnehmen sollen.
4. Klicken Sie auf den button "Rennen starten".
5. Bestätigen Sie ggf. den Dialog mit dem Hardwarehinweis.
6. Die Messung starten, sobald die Lichtschranke durchfahren wird. Es ist vorgesehen, dass nur ein Team gleichzeitig auf der Rennstrecke fährt.
7. Sobald alle Teams die Strecke absolviert haben, gelangen Sie in das Hauptfenster zurück, wo Ihnen eine Zusammenfassung des Rennens angezeigt wird.

### <a name="Erfolgte">Erfolgte Rennen  - Detailansicht</a> ###
1. Klicken Sie auf den Reiter "Erfolgte Rennen".
2. Wählen Sie das entsprechende Rennen aus der Liste.

Am unteren rechten Fensterrand haben Sie nun folgende Möglichkeiten:

#### <a name="exportieren">Rennen exportieren</a> ####

Es besteht die Möglichkeit ein Rennen in CSV- (Tabellenkalkulation) oder XML-Format (für späteren Import oder zur Weiterverarbeitung) zu exportieren.

  -  Klicken Sie hierzu auf den Button "Rennen exportieren" und folgen Sie den weiteren Anweisungen.
  
#### <a name="Siegerurkunden">Siegerurkunden erstellen</a> ####
Es besteht die Möglichkeit für ein Rennen Siegerurkunden im PDF-Format zu erstellen.

  -  Klicken Sie hierzu auf den Button "Urkunden als PDF erstellen" und folgen Sie den weiteren Anweisungen.

#### <a name="rlöschen">Rennen löschen</a> ####
Es besteht die Möglichkeit ein Rennen zu löschen.

  -  Klicken Sie hierzu auf den Button "Rennen löschen". Sollten Sie es sich anders überlegt haben, können sie am unteren rechten Fensterrand auf den Button "Rückgängig" klicken (erscheint für 3 Sekunden), das Rennen wird dann wieder in der Liste erscheinen.

### <a name="importieren">Datenset importieren</a> ###
Es besteht die Möglichkeit ein oder mehrere Rennen aus eine XML-Datei zu importieren.

1. Klicken Sie auf das Menü "Programm".
2. Klicken Sie auf den Menüpunkt "Datenset importieren" und folgen Sie den Anweisungen.

### <a name="dexportieren">Datenset exportieren</a> ###
Es besteht die Möglichkeit alle Rennen in eine XML-Datei zu exportieren.
1. Klicken Sie auf das Menü "Programm".
2. Klicken Sie auf den Menüpunkt "Datenset exportieren" und folgen Sie den Anweisungen.

### <a name="Einstellungen">Einstellungen ändern</a> ###
1. Klicken Sie auf das Menü "Extras".
2. Klicken Sie auf den Menüpunkt "Einstellungen".

Sie haben nun folgende Einstellungsmöglichkeiten.

  -  Ändern der Sprache
    - Wählen Sie aus Deutsch oder Englisch
  - Ändern des Programmthemas
    - Wählen Sie aus Light_Theme oder Dark_Theme
  - Aktivieren/Deaktivieren der Programmklänge
  - Aktivieren/Deaktivieren des Hardwarehinweises, welcher beim Starten eines neuen Rennens erscheint

Bestätigen Sie Ihre Änderungen mit "ok" oder "anwenden" bzw. verwerfen Sie Ihre Änderungen durch einen Klick auf "abbrechen".

# <a name="Developers">Developers Guide</a> #

## <a name="Programmstruktur">Programmstruktur</a> ##

 Zur besseren Übersicht, über die Programmstruktur, dient das <a href="../klassendiagram.png" target="_blank">Klassendiagramm</a>.

## <a name="Entwicklungsumgebungen">Verwendete Entwicklungsumgebungen</a> ##

Sowohl der Java-Client, als auch die Hardwaresteuerung wurden in Eclipse erstellt und können, in den entsprechenden Versionen, als Projekt importiert werden.


### <a name="LegoRacing_ClientLegoRacing_Client">LegoRacing_Client</a> ###

Erstellt unter: 
  -  Name:    Eclipse Java EE IDE for Web Developers
 
  -  Version: Photon Release (4.8.0)
  
  -  Kostenfreier Download: <a href="https://www.eclipse.org/downloads/packages/" target="_blank">https://www.eclipse.org/downloads/packages/</a> (Stand: 2018-08-31)

#### <a name="Plugins">verwendete Plugins</a> ####

  -  e(fx)clipse (eclipse-Marketplace)
  
  -  Eclox (eclipse-Marketplace)
  
  -  ObjectAid (<a href="http://www.objectaid.com/installation" target="_blank">http://www.objectaid.com/installation</a> (Stand: 2018-08-31)
		
### <a name="LegoRacing_Hardwaresteuerung">LegoRacing_Hardwaresteuerung</a> ###


Erstellt unter: 
  -  Name:    Sloeber, the Eclipse IDE for Arduino Developers
 
  -  Version: Beryllium Release (4.2)
  
  -  Kostenfreier Download_ <a href="https://eclipse.baeyens.it/" target="_blank">https://eclipse.baeyens.it/</a> (Stand: 2018-08-31)
  
## <a name="Persistenz">Persistenz und Speicherorte(nach Installation)</a> ##

### <a name="Datenbank">Datenbank</a> ###
Zur dauerhaften Speicherung der Rennen und Teams wird eine SQLite-Datenbank verwendet. Diese wird für jeden Benutzer des Systems separat angelegt, sie befindet sich unter: $HOME/.local/share/HOST_LegoRacing

### <a name="Programmeinstellungen">Programmeinstellungen</a> ###
Zur Speicherung von Programmeinstellungen wird "Core Java Preferences API" verwendet. Die Daten werden, für jeden Systembenutzer separat, unter: $HOME/.java/.userPrefs/ abgelegt.

### <a name="Programmbibliotheken">Programmbibliotheken</a> ###
	Die Programmbibliotheken werden unter: /usr/lib/HOST_LegoRacing/ abgelegt.
	
### <a name="Icon">Icon</a> ###
	Das Programmicon wird unter: /usr/share/pixmaps/HOST_LegoRacing.png abgelegt.
	
### <a name="Menü">Menü-Config</a> ###
	Die Konfigurationsdatei zum erstellen des Menüeintrages befindet sich unter: /usr/share/applications/HOST_LegoRacing.desktop
	
### <a name="Programmdateien">Programmdateien</a> ###
	Das eigentliche Programm wird unter: /usr/bin/ abgelegt.
	

# <a name="Hardwaremodifikation">Hardwaremodifikation</a> #

Eine Kommunikation vom Pc zur Stoppuhr LSU 100 ist herstellerseitig nicht vorgesehen, dies wurde mit Hilfe eines Arduino-NANO nachimplementiert. 

Die Verkabelung kann dem <a href="../drahtplan.png" target="_blank">Verdrahtungsplan</a> entnommen werden.

 */