package application;

import java.util.prefs.Preferences;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
/**
 * 
 * @author Robert Pfeiffer
 *
 * Dieser Controller ist mit der Viewvorlage "HardwareAlertGui.fxml" verknüpft.
 */
public class HardwareAlertController {
	@FXML
	private CheckBox checkbox_alert_dont_show_again;

	@FXML
	public void initialize() {
	}

	/**
	 * Diese Methode wird durch ein Button-Klick aufgerufen. Sie speichert den Wert der Checkbox "checkbox_alert_dont_show_again" und schließt die Stage.
	 */
	public void onClickBtnOk() {
		boolean showAgain = checkbox_alert_dont_show_again.isSelected();
		Preferences.userNodeForPackage(PreferencesController.class).putBoolean("checkboxStartingDialog", !showAgain);
		checkbox_alert_dont_show_again.getScene().getWindow().hide();
	}
}
