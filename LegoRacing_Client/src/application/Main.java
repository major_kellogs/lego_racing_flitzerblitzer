package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import backend.DBController;
import backend.Rennen;
import backend.Team;
import backend.USBConnection;
import backend.USBListener;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;

/*
 * TODO's
 * 
 * - Spracheinstellungen
 * - allgemeine Codequalität erhöhen
 * 
 * :::OPTIONAL::: 
 * - tu-es-trotzdem knopf, um mit nur einem Team ein Rennen starten zu können
 * - stärkere response einbinden, z.B.: durch verstärkten Einsatz der snackbar
 * 
 */

/**
 * 
 * @author Robert Pfeiffer
 *
 */
public class Main extends Application {
	public static Stage primaryStage;
	public static Stage preferencesStage = new Stage();
	public static Stage runningRaceStage = new Stage();
	public static Stage alertStage;
	private static Main main;

	public static Rennen runningRace;
	public static IntegerProperty runningRacePosition = new SimpleIntegerProperty(0);
	public static BooleanProperty dataChangedFlag = new SimpleBooleanProperty(false);
	public static ArrayList<String> serialPorts = new ArrayList<>();
	public static StringProperty lsu100Port = new SimpleStringProperty("");
	public static StringProperty arduinoPort = new SimpleStringProperty("");
	public static DoubleProperty runningRaceDuration = new SimpleDoubleProperty(0);
	public static USBConnection lsu100;
	public static USBConnection arduino;
	private USBListener usbListener = new USBListener();
	public static ResourceBundle languageBundle = ResourceBundle.getBundle("language",
			Locale.forLanguageTag(Preferences.userNodeForPackage(PreferencesController.class).get("lang", "de")));
	public static String raceNameForExport = "";
	public static StringProperty progressText = new SimpleStringProperty("");
	public static Rennen recentlyDeletedRace;

	public static boolean racelisteners;
	public static boolean roundIsRunning;

	@Override
	public void start(Stage _primaryStage) {
		DBController.initDB();
		main = this;
		primaryStage = _primaryStage;
		showMainGui();
	}

	/**
	 * Diese Methode zeigt die MainGui an.
	 */
	private static void showMainGui() {
		try {
			languageBundle = ResourceBundle.getBundle("language",
					Locale.forLanguageTag(Preferences.userNodeForPackage(PreferencesController.class).get("lang", "de")));
			Parent root = FXMLLoader.load(main.getClass().getResource("MainGui.fxml"), languageBundle);
			Scene scene = new Scene(root, 1280, 800);
			scene.getStylesheets()
					.add(main.getClass().getResource("../themes/"
							+ Preferences.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme")
							+ ".css").toExternalForm());
			primaryStage.setTitle("LegoRacing - FlitzerBlitzer");
			primaryStage.getIcons().add(new Image("images/logo.png"));
			primaryStage.minWidthProperty().set(1024);
			primaryStage.minHeightProperty().set(700);
			primaryStage.setScene(scene);
			primaryStage.setOnCloseRequest(e -> {
				if (preferencesStage != null && preferencesStage.isShowing())
					preferencesStage.hide();
			});
			primaryStage.show();
		} catch (Exception e) {
			System.out.println("main exception");
			e.printStackTrace();
		}
	}
	
/**
 * Diese Methode zeigt die PreferencesGui an und stellt sicher, dass die MainGui solange deaktiviert wird.
 */
	public static void showPrefGui() {
		try {
			Main.primaryStage.focusedProperty().addListener(new ChangeListener<Boolean>() {

				@Override
				public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
					if (newValue) {
						preferencesStage.toFront();
					}
				}
			});

			Parent root = FXMLLoader.load(main.getClass().getResource("PreferencesGui.fxml"), languageBundle);
			Scene scene = new Scene(root);
			scene.getStylesheets()
					.add(main.getClass().getResource("../themes/"
							+ Preferences.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme")
							+ ".css").toExternalForm());
			Main.preferencesStage.setTitle("LegoRacing - FlitzerBlitzer");
			Main.preferencesStage.setScene(scene);
			Main.preferencesStage.setResizable(false);
			Main.preferencesStage.show();
		} catch (Exception e) {
			System.out.println("prefs exception");
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode zeigt die RunningRaceGui an.
	 */
	public static void showRunningRaceStage() {
		if (runningRaceStage != null && runningRaceStage.isShowing())
			runningRaceStage.hide();
		try {
			runningRaceStage = new Stage();
			runningRaceStage.getIcons().add(new Image("images/logo.png"));
			Parent root;
			root = FXMLLoader.load(main.getClass().getResource("runningRaceGui.fxml"), Main.languageBundle);
			Main.runningRaceStage.setTitle("LegoRacing - Rennen läuft | " + Main.runningRace.getName());
			Scene scene = new Scene(root, 800, 579);
			scene.getStylesheets()
					.add(main.getClass().getResource("../themes/"
							+ Preferences.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme")
							+ ".css").toExternalForm());
			Main.runningRaceStage.setScene(scene);

			Main.runningRaceStage.show();
			if (primaryStage != null && primaryStage.isShowing())
				primaryStage.hide();
		} catch (Exception e) {
			System.out.println("runnning exception");
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode schließt die RunningRaceGui und ruft showMainGui() auf.
	 */
	public static void resetMainGui() {
		if (runningRaceStage != null && runningRaceStage.isShowing()) {
			runningRaceStage.hide();
		}
		if (primaryStage != null && primaryStage.isShowing())
			primaryStage.hide();
		showMainGui();

	}

	/**
	 * Diese Methode ruft resetMainGui() und bei Bedarf PreferencesGui auf.
	 */
	public static void resetGui() {
		resetMainGui();
		if (preferencesStage != null && preferencesStage.isShowing()) {
			preferencesStage.hide();
			showPrefGui();
		}
	}

	public static void main(String[] args) {
		//Pfad für Datenbank überprüfen und ggf. anlegen.
		File dbDir = new File(System.getProperty("user.home") + "/.local/share/HOST_LegoRacing/");
		if (!dbDir.exists())
			dbDir.mkdirs();

		launch(args);
	}

	/**
	 * Diese Methode wird beim Beenden der Software aufgerufen und 
	 */
	@Override
	public void stop() {
		usbListener.stopListening();
		try {
			Main.lsu100.reader.join(200, 0);
		} catch (Exception e) {
			System.out.println("joining thread failed ... already closed");
		}
	}
}
