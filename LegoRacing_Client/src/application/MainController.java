package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.prefs.Preferences;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.jfoenix.controls.JFXSnackbar;
import backend.*;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * 
 * @author Robert Pfeiffer
 *
 *         Dieser Controller ist mit der Viewvorlage "MainGui.fxml" verknüpft.
 */
public class MainController {
	@FXML
	private AnchorPane basePane;
	@FXML
	private AnchorPane overlayPane;
	@FXML
	private HBox hbProgress;
	@FXML
	private MenuBar menuBar;
	@FXML
	private TreeView<String> tvRaceOverview;
	@FXML
	private Label LabelConnected;
	@FXML
	private Label LabelConnectedArduino;
	@FXML
	private Label labelRacedata;
	@FXML
	private Label labelDatum;
	@FXML
	private Label labelUhrzeit;
	@FXML
	private Label labelProgress;
	@FXML
	private TextField tfTeamName;
	@FXML
	private TextField tfRaceName;
	@FXML
	private TextArea taTeamMember;
	@FXML
	private Button btnNewAddTeam;
	@FXML
	private Button btnTeamAddTeam;
	@FXML
	private Button btnTeamDelTeam;
	@FXML
	private Button btnTeamEditTeam;
	@FXML
	private Button btnTeamSave;
	@FXML
	private Button btnTeamReset;
	@FXML
	private Button btnNewRaceStart;
	@FXML
	private Button btnCloseNewTeam;
	@FXML
	private ListView<String> lvNewTeamView;
	@FXML
	private TreeView<String> tvTeamTeamView;
	@FXML
	private SplitPane spRaceOverview;
	@FXML
	private SplitPane spTeam;
	@FXML
	private TableView<String> tableRaceData;
	@FXML
	private TableColumn<String, ?> tcNumber;
	@FXML
	private TableColumn<String, ?> tcName;
	@FXML
	private TableColumn<String, ?> tcDuration;
	@FXML
	private TabPane tabPane;
	@FXML
	private ProgressIndicator piProgress;

	@FXML
	private ImageView ivConnection;
	@FXML
	private ImageView ivConnectionArduino;
	private Image disconnected = new Image("images/disconnected.png");
	private Image connected = new Image("images/connected.png");

	private boolean entersFromNewRace = false;
	private Team editingteam = null;
	private ArrayList<String> teamsForNewRace = new ArrayList<String>();

	@FXML
	private void initialize() {
		tableRaceData.setSelectionModel(null);
		tcNumber.setCellValueFactory(new PropertyValueFactory<>("Number"));
		tcName.setCellValueFactory(new PropertyValueFactory<>("Name"));
		tcDuration.setCellValueFactory(new PropertyValueFactory<>("Duration"));

		setGraphics();
		initListeners();
		Platform.runLater(() -> {
			spRaceOverview.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
		});
		fillOverviewList();
		fillTeamViews();
		handleStartButton();
		handleLabelConnected();

		Platform.runLater(() -> {
			tvRaceOverview.getSelectionModel().select(0);
			tvRaceOverview.requestFocus();

			spTeam.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
		});

	}

	/**
	 * Diese Methode setzt initial die Grafiken für Buttons und ImageViews.
	 */
	private void setGraphics() {
		btnCloseNewTeam.setGraphic(new ImageView(new Image("images/close_mini.png")));
		btnNewAddTeam.setGraphic(new ImageView(new Image("images/add.png")));
		btnTeamAddTeam.setGraphic(new ImageView(new Image("images/add.png")));
		btnTeamDelTeam.setGraphic(new ImageView(new Image("images/del.png")));
		btnTeamEditTeam.setGraphic(new ImageView(new Image("images/edit.png")));
		ivConnection.setImage(disconnected);
		ivConnectionArduino.setImage(disconnected);
	}

	/**
	 * Diese Methode setzt alle benötigten Listener.
	 */
	private void initListeners() {

		basePane.widthProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				menuBar.setPrefWidth((double) newValue);
				if (tvRaceOverview.getSelectionModel().getSelectedIndex() != -1 && tvRaceOverview
						.getTreeItem(tvRaceOverview.getSelectionModel().getSelectedIndex()).getChildren().size() != 0) {
					spRaceOverview.setDividerPosition(0, 0.5);
					Platform.runLater(() -> {
						spRaceOverview.lookupAll(".split-pane-divider").stream()
								.forEach(div -> div.setMouseTransparent(false));
					});

				} else {
					closeRaceoverview();
				}
			}
		});

		basePane.heightProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if (!tfTeamName.isFocused() && !taTeamMember.isFocused() && !btnTeamSave.isFocused()
						&& !btnTeamReset.isFocused()) {
					spTeam.setDividerPosition(0, 1);
					Platform.runLater(() -> {
						spTeam.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
					});
				}

			}
		});

		tvRaceOverview.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (tvRaceOverview.getSelectionModel().getSelectedIndex() != -1 && tvRaceOverview
					.getTreeItem(tvRaceOverview.getSelectionModel().getSelectedIndex()).getChildren().size() != 0) {
				spRaceOverview.setDividerPosition(0, 0.5);
				publishRaceData(tvRaceOverview.getSelectionModel().getSelectedItem().getValue());
			} else {
				spRaceOverview.setDividerPosition(0, 1);
			}
		});

		tvRaceOverview.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE) {
				closeRaceoverview();
			}
		});

		tableRaceData.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE) {
				closeRaceoverview();
			}
		});

		tvTeamTeamView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				btnTeamDelTeam.setDisable(false);
				btnTeamEditTeam.setDisable(false);
			}
			if(newSelection != null && newSelection.isLeaf()) {
				btnTeamDelTeam.setDisable(true);
				btnTeamEditTeam.setDisable(true);
			}
		});

		tvTeamTeamView.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE) {
				tvTeamTeamView.getSelectionModel().clearSelection();
				btnTeamDelTeam.setDisable(true);
				btnTeamEditTeam.setDisable(true);
			}
		});

		tfTeamName.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE) {
				onClickBtnTeamReset();
				spTeam.setDividerPosition(0, 1);
				Platform.runLater(() -> {
					spTeam.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
				});
				if (entersFromNewRace) {
					tabPane.getSelectionModel().select(1);
					entersFromNewRace = false;
				}
			}
		});

		tfRaceName.setOnKeyPressed(e -> {
			if (!tfRaceName.getText().isEmpty() && !Main.lsu100Port.get().isEmpty()) {
				btnNewRaceStart.setDisable(false);
			} else {
				btnNewRaceStart.setDisable(true);
				tfRaceName.setStyle("-fx-border-color: none");
			}
		});

		taTeamMember.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.ESCAPE) {
				onClickBtnTeamReset();
				spTeam.setDividerPosition(0, 1);
				Platform.runLater(() -> {
					spTeam.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
				});
				if (entersFromNewRace) {
					tabPane.getSelectionModel().select(1);
					entersFromNewRace = false;
				}
			}
		});
		Main.dataChangedFlag.addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				fillOverviewList();
			}
		});

		Main.lsu100Port.addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				handleStartButton();
				handleLabelConnected();
			}
		});

		Main.arduinoPort.addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				handleStartButton();
				handleLabelConnected();
			}
		});

		Main.preferencesStage.showingProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (newValue == true)
					overlayPane.setVisible(true);
				else
					overlayPane.setVisible(false);

			}
		});

		Main.progressText.addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.isEmpty()) {
					hbProgress.setVisible(false);
				} else {
					labelProgress.setText(Main.progressText.get());
					hbProgress.setVisible(true);
				}

			}
		});
	}

	/**
	 * Diese Methode händelt die Verbindungsindikatoren, in der Statusleiste, am unteren Fensterrand.
	 */
	private void handleLabelConnected() {
		if (Main.lsu100Port.get().isEmpty()) {
			Platform.runLater(() -> {
				ivConnection.setImage(disconnected);
				LabelConnected.setText(Main.languageBundle.getString("lable_main_lsu_connection_disconnected"));
			});
		} else {
			Platform.runLater(() -> {
				ivConnection.setImage(connected);
				LabelConnected.setText(
						Main.languageBundle.getString("lable_main_lsu_connection_connected") + Main.lsu100Port.get());
			});
		}
		if (Main.arduinoPort.get().isEmpty()) {
			Platform.runLater(() -> {
				ivConnectionArduino.setImage(disconnected);
				LabelConnectedArduino
						.setText(Main.languageBundle.getString("lable_main_arduino_connection_disconnected"));
			});
		} else {
			Platform.runLater(() -> {
				ivConnectionArduino.setImage(connected);
				LabelConnectedArduino.setText(Main.languageBundle.getString("lable_main_arduino_connection_connected")
						+ Main.arduinoPort.get());
			});
		}

	}

	/**
	 * Diese Methode händelt die Verfügbarkeit des Buttons "btnNewRaceStart".
	 */
	private void handleStartButton() {
		if (!Main.lsu100Port.get().isEmpty() && !tfRaceName.getText().isEmpty())
			btnNewRaceStart.setDisable(false);
		else
			btnNewRaceStart.setDisable(true);
	}

	/**
	 * Diese Methode befüllt die TreeView mit der Übersicht aller erfolgten Rennen.
	 */
	private void fillOverviewList() {
		ArrayList<TreeItem<String>> treeData = new ArrayList<TreeItem<String>>();

		for (Rennen x : DBController.queryForAllRaces()) {
			TreeItem<String> temp = new TreeItem<String>(
					x.getName() + " - [" + x.getDatumAsDate() + " | " + x.getUhrzeit() + "]");
			ArrayList<TreeItem<String>> tempList = new ArrayList<TreeItem<String>>();
			for (Rennleistung y : x.getRennLeistungen()) {
				StringBuilder sb = new StringBuilder(y.getTeam().getName() + " -");
				for (String z : y.getTeam().getMitglieder())
					sb.append(" " + z + ", ");

				tempList.add(new TreeItem<String>(sb.toString()));
			}
			temp.getChildren().addAll(tempList);
			treeData.add(temp);
		}
		TreeItem<String> rootItem = new TreeItem<String>("root");
		rootItem.getChildren().addAll(treeData);
		tvRaceOverview.setRoot(rootItem);
		tvRaceOverview.setShowRoot(false);
		tvRaceOverview.getSelectionModel().clearAndSelect(0);
		tvRaceOverview.requestFocus();
	}

	/**
	 * Diese Methode befüllt die ListView zur Auswahl der an einem Rennen teilnehmenden Teams, sowie die Treeview in der Teamverwaltung.
	 */
	private void fillTeamViews() {
		lvNewTeamView.getItems().clear();
		ArrayList<TreeItem<String>> treeData = new ArrayList<>();
		for (Team x : DBController.queryForAllTeams()) {
			lvNewTeamView.getItems().add(x.getName());
			CheckBoxTreeItem<String> tempTeam = new CheckBoxTreeItem<String>(x.toString());
			ArrayList<TreeItem<String>> tempMitglieder = new ArrayList<>();
			for (String y : x.getMitglieder()) {
				tempMitglieder.add(new CheckBoxTreeItem<String>(y));
			}
			tempTeam.getChildren().addAll(tempMitglieder);
			treeData.add(tempTeam);
		}

		CheckBoxTreeItem<String> rootItem = new CheckBoxTreeItem<String>("root");
		rootItem.getChildren().addAll(treeData);

		lvNewTeamView.setCellFactory(CheckBoxListCell.forListView(new Callback<String, ObservableValue<Boolean>>() {
			@Override
			public ObservableValue<Boolean> call(String item) {
				BooleanProperty observable = new SimpleBooleanProperty();
				observable.addListener((obs, wasSelected, isNowSelected) -> {
					if (isNowSelected)
						teamsForNewRace.add(item);
					else
						teamsForNewRace.remove(item);
				});
				return observable;
			}
		}));

		tvTeamTeamView.setRoot(rootItem);

		tvTeamTeamView.setShowRoot(false);
	}

	/**
	 * Diese Methode befüllt die Felder zum Bearbeiten eines Teams.
	 */
	private void fillTeamFields() {
		if (editingteam != null) {
			tfTeamName.setText(editingteam.getName());
			StringBuilder sb = new StringBuilder();
			for (String x : editingteam.getMitglieder())
				sb.append(x + System.lineSeparator());
			taTeamMember.setText(sb.toString());
		}
	}

	/**
	 * Diese Methode schließt die Detailansicht eines erfolgten Rennens.
	 */
	private void closeRaceoverview() {
		spRaceOverview.setDividerPosition(0, 1);
		tvRaceOverview.getSelectionModel().select(-1);
		Platform.runLater(() -> {
			spRaceOverview.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
		});
	}

	/**
	 * Diese Methode schließt das Formular zum Anlegen/Bearbeiten eines Teams.
	 */
	private void closeTeamFields() {
		spTeam.setDividerPosition(0, 1);
		tvTeamTeamView.getSelectionModel().select(-1);
		btnTeamDelTeam.setDisable(true);
		btnTeamEditTeam.setDisable(true);

		if (entersFromNewRace) {
			tabPane.getSelectionModel().select(1);
			entersFromNewRace = false;
		}
		editingteam = null;
		Platform.runLater(() -> {
			spTeam.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(true));
		});
	}

	/**
	 * Diese Methode leert die Felder des Formulars zum Anlegen/Bearbeiten eines Teams.
	 */
	private void clearTeamFields() {
		tfTeamName.setText("");
		taTeamMember.setText("");
	}

	/**
	 * Diese Methode befüllt die Detailansicht eines erfolgten Rennens mit den entsprechenden Daten.
	 * 
	 * @param _listItem - der Name(Inhalt) des augewählten Listeneintrages
	 */
	private void publishRaceData(String _listItem) {

		_listItem = _listItem.substring(0, _listItem.length() - 26);

		Rennen rennen = DBController.queryForSpecificRace(_listItem);

		labelDatum.setText(rennen.getDatumAsDate());
		labelUhrzeit.setText(rennen.getUhrzeit());

		ArrayList<Rennleistung> platzierungen = (ArrayList<Rennleistung>) rennen.getRennLeistungen().clone();
		Collections.sort(platzierungen, new Comparator<Rennleistung>() {

			@Override
			public int compare(Rennleistung o1, Rennleistung o2) {
				if (o1.getDauer() > o2.getDauer())
					return 1;
				else
					return -1;
			}
		});
		final ObservableList data = FXCollections.observableArrayList();

		for (int i = 0; i < platzierungen.size(); i++)
			data.add(new TableData(Integer.toString(i + 1), platzierungen.get(i).getTeam().getName(),
					Rennzeit.convertMillisToString(platzierungen.get(i).getDauer())));

		tableRaceData.setItems(data);
		labelRacedata.setText(rennen.getName());
	}

	/*
	 * Buttonactions
	 */

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie setzt die Felder zum Anlegen/Bearbeiten eines Teams zurück.
	 */
	public void onClickBtnTeamReset() {
		clearTeamFields();
		fillTeamFields();
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie öffnet das Formular zum Anlegen eines neuen Teams.
	 */
	public void onClickBtnTeamAdd() {
		clearTeamFields();
		spTeam.setDividerPosition(0, 0.3);
		Platform.runLater(() -> {
			spTeam.lookupAll(".split-pane-divider").stream().forEach(div -> div.setMouseTransparent(false));
		});
		tfTeamName.requestFocus();
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie löscht das ausgewählte Team.
	 */
	public void onClickBtnTeamDelete() {
		DBController.deleteTeam(getTeamFromTeamListSelection());
		btnTeamDelTeam.setDisable(true);
		btnTeamEditTeam.setDisable(true);
		fillTeamViews();
		clearTeamFields();
		closeTeamFields();
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie öffnet das Formular zum Bearbeiten eines Teams.
	 */
	public void onClickBtnTeamEdit() {
		editingteam = getTeamFromTeamListSelection();
		onClickBtnTeamAdd();
		fillTeamFields();
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie speichert ein Team.
	 */
	public void onClickBtnTeamSave() {
		if (tfTeamName.getText().isEmpty() || taTeamMember.getText().isEmpty()) {
			JFXSnackbar notification = new JFXSnackbar(basePane);
			notification.getPopupContainer().setOnMouseClicked(f -> {
				notification.unregisterSnackbarContainer(basePane);
			});

			notification.setPrefWidth(Main.primaryStage.getWidth());
			notification.getChildren().get(0).setStyle("-fx-background-color: #C00000D0");

			notification.show(Main.languageBundle.getString("empty_fields"), 3500);
		} else {
			try {
				Team tempTeam = new Team(tfTeamName.getText());
				for (String member : taTeamMember.getText().trim().split(System.getProperty("line.separator")))
					tempTeam.addMitglied(member);
				if (editingteam == null) {
					// INSERT
					DBController.insertTeam(tempTeam);
				} else {
					// UPDATE
					DBController.updateTeam(editingteam.getName(), tempTeam);
				}

				fillTeamViews();
				onClickBtnTeamReset();
				closeTeamFields();
			} catch (DuplicateEntryException e) {
				JFXSnackbar notification = new JFXSnackbar(basePane);
				notification.getPopupContainer().setOnMouseClicked(f -> {
					notification.unregisterSnackbarContainer(basePane);
				});

				notification.setPrefWidth(Main.primaryStage.getWidth());
				notification.getChildren().get(0).setStyle("-fx-background-color: #C00000D0");

				notification.show(Main.languageBundle.getString("duplicate_team"), 3500);
			}

		}
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie öffnet das Formular zum Anlegen eines neuen Teams und sorgt dafür, dass der Benutzer nach dem speichern/abbrechen zurück zur Maske "Neues Rennen" geführt wird.
	 */
	public void onClickBtnNewRaceAdd() {
		tabPane.getSelectionModel().select(2);
		entersFromNewRace = true;
		onClickBtnTeamAdd();
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie setzt die Felder der Maske "Neues Rennen" zurück.
	 */
	public void onClickBtnNewRaceReset() {
		clearTeamFields();
		fillTeamViews();

	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie überprüft die Benutzeringaben in der Makse "Neues Rennen" auf Gültigkeit und ruft ggf. startNewRaceScene() auf.
	 */
	public void onClickBtnNewRaceStart() {
		String raceName = tfRaceName.getText();
		try {
			if (!raceName.isEmpty()) {

				if (DBController.checkRaceNameExists(raceName))
					throw new DuplicateEntryException(
							"Dieses Rennen gab es bereits, bitte wählen Sie einen anderen Namen.");

				Main.runningRace = new Rennen(raceName);
				Main.runningRacePosition.set(0);
				for (String x : teamsForNewRace) {
					Team y = DBController.queryForSpecificTeam(x);
					if (y != null)
						Main.runningRace.addRennLeistungen(new Rennleistung(y));
				}
				if (Main.runningRace.getRennLeistungen().size() < 2) {
					Main.runningRace = null;
					throw new EmptyInputException("Teamauswahl zu klein");
				}
				startNewRaceScene();
			} else {
				throw new EmptyInputException("Der Name des Rennens darf nicht leer sein.");
			}
		} catch (DuplicateEntryException e) {
			JFXSnackbar notification = new JFXSnackbar(basePane);
			notification.getPopupContainer().setOnMouseClicked(f -> {
				notification.unregisterSnackbarContainer(basePane);
			});

			notification.setPrefWidth(Main.primaryStage.getWidth());
			notification.getChildren().get(0).setStyle("-fx-background-color: #C00000D0");

			notification.show(Main.languageBundle.getString("duplicate_race"), 3500);
		} catch (EmptyInputException e) {
			if (e.getMessage().equals("Teamauswahl zu klein")) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle(Main.languageBundle.getString("Alert_not_enough_teams_selected_title"));
				alert.setHeaderText(Main.languageBundle.getString("Alert_not_enough_teams_selected1"));
				alert.getDialogPane().setStyle("-fx-pref-width: 400px; -fx-pref-height: 180px;");
				alert.setContentText(Main.languageBundle.getString("Alert_not_enough_teams_selected2"));

				// TODO "Tu-es-trotzdem-Knopf bei nur einem Team"!

				alert.showAndWait();
			}
		}
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen und bietet die Möglichkeit ein Rennen als Datei zu exportieren.
	 */
	public void onClickBtnExportRace() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Export -> " + labelRacedata.getText());
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter(".csv", "*.csv"), new FileChooser.ExtensionFilter(".xml", "*.xml"));
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		File filePath = new File(fileChooser.showSaveDialog(new Stage()).getAbsoluteFile()
				+ fileChooser.getSelectedExtensionFilter().getDescription());
		Rennen exportRennen = DBController.queryForSpecificRace(labelRacedata.getText());
		if (exportRennen != null) {
			String exportContent = "";
			if (fileChooser.getSelectedExtensionFilter().getDescription().equals(".csv")) {
				while (filePath.getPath().contains(".csv.csv"))
					filePath = new File(filePath.getPath().replaceAll(".csv.csv", ".csv"));
				exportContent = FiletransferHelper.raceToCSVContent(exportRennen);
			} else {
				while (filePath.getPath().contains(".xml.xml"))
					filePath = new File(filePath.getPath().replaceAll(".xml.xml", ".xml"));
				exportContent = FiletransferHelper.raceToXMLContent(exportRennen);
			}
			try {
				FiletransferHelper.writeTextFile(filePath, exportContent);
				final String absolutePath = filePath.getParentFile().getAbsolutePath();
				// Notification: Datei erfolgreich angelegt
				final JFXSnackbar notification = new JFXSnackbar(basePane);
				notification.setPrefWidth(Main.primaryStage.getWidth());
				notification.getPopupContainer().setOnMouseClicked(e -> {
					notification.unregisterSnackbarContainer(basePane);
				});
				EventHandler action = new EventHandler() {

					@Override
					public void handle(Event event) {
						try {
							Runtime.getRuntime().exec("xdg-open " + absolutePath);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						notification.unregisterSnackbarContainer(basePane);
					}
				};
				notification.show(filePath.getName() + Main.languageBundle.getString("succesfully_created"),
						Main.languageBundle.getString("open_folder"), 3500, action);

			} catch (Exception e) {
				// Notification: Datei nicht angelegt
				JFXSnackbar notification = new JFXSnackbar(basePane);
				notification.getPopupContainer().setOnMouseClicked(f -> {
					notification.unregisterSnackbarContainer(basePane);
				});
				notification.setPrefWidth(Main.primaryStage.getWidth());
				notification.getChildren().get(0).setStyle("-fx-background-color: #C00000D0");
				notification.show(filePath.getName() + Main.languageBundle.getString("cant_create"), 4500);

				e.printStackTrace();
			}

		}
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen und bietet die Möglichkeit Siegerurkunden eines Rennens als PDF-Datei zu erstellen.
	 */
	public void onClickBtnRaceToPDF() {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Export as PDF");
		chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".pdf", "*.pdf"));
		chooser.setInitialDirectory(new File(System.getProperty("user.home")));
		File exportFile = new File(chooser.showSaveDialog(new Stage()).getAbsolutePath()
				+ chooser.getSelectedExtensionFilter().getDescription());

		while (exportFile.getPath().contains(".pdf.pdf"))
			exportFile = new File(exportFile.getPath().replaceAll(".pdf.pdf", ".pdf"));
		final File xFile = exportFile;
		Main.progressText.set(Main.languageBundle.getString("progress_write_pdf"));
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					PDFCreator pdf = new PDFCreator(xFile, DBController.queryForSpecificRace(labelRacedata.getText()));

					final String absolutePath = xFile.getParentFile().getAbsolutePath();
					// Notification: Datei erfolgreich angelegt
					Platform.runLater(() -> {
						final JFXSnackbar notification = new JFXSnackbar(basePane);
						notification.setPrefWidth(Main.primaryStage.getWidth());
						notification.getPopupContainer().setOnMouseClicked(e -> {
							notification.unregisterSnackbarContainer(basePane);
						});
						EventHandler action = new EventHandler() {

							@Override
							public void handle(Event event) {
								try {
									Runtime.getRuntime().exec("xdg-open " + absolutePath);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								notification.unregisterSnackbarContainer(basePane);
							}
						};
						Main.progressText.set("");
						notification.show(xFile.getName() + Main.languageBundle.getString("succesfully_created"),
								Main.languageBundle.getString("open_folder"), 3500, action);

					});

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).start();

	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie löscht das in der Detailsansicht angezeigte Rennen und bietet kurzzeitig die Möglichkeit diesen Vorgang wieder rückgängig zu machen.
	 */
	public void onClickBtnDeleteRace() {

		final String raceName = labelRacedata.getText();
		Main.recentlyDeletedRace = DBController.queryForSpecificRace(raceName);
		DBController.deleteRace(raceName);
		fillOverviewList();

		final JFXSnackbar notification = new JFXSnackbar(basePane);
		notification.setPrefWidth(Main.primaryStage.getWidth());
		notification.getPopupContainer().setOnMouseClicked(e -> {
			notification.unregisterSnackbarContainer(basePane);
		});
		EventHandler action = new EventHandler() {

			@Override
			public void handle(Event event) {
				DBController.insertRace(Main.recentlyDeletedRace);

				notification.unregisterSnackbarContainer(basePane);
				JFXSnackbar postNotification = new JFXSnackbar(basePane);
				postNotification.getPopupContainer().setOnMouseClicked(e -> {
					postNotification.unregisterSnackbarContainer(basePane);
				});
				postNotification.setPrefWidth(Main.primaryStage.getWidth());
				postNotification.show(raceName + Main.languageBundle.getString("succesfully_recreated"), 3000);
				fillOverviewList();
			}
		};
		notification.show(raceName + Main.languageBundle.getString("succesfully_deleted"),
				Main.languageBundle.getString("undo"), 3500, action);

	}

	/**
	 * Diese Methode regelt das Verhalten, wenn auf den TAB "Neues Rennen" geklickt wird.
	 */
	public void onClickTabNewRace() {
		Platform.runLater(() -> {
			tfRaceName.requestFocus();
		});
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie schließt das Formular zum Anlegen/Bearbeiten eines Teams.
	 */
	public void onClickBtnCloseNewTeam() {
		clearTeamFields();
		closeTeamFields();
	}

	/**
	 * Diese Methode wird durch Klick auf einen Menüpunkt aufgerufen, sie bietet die Möglichkeit bereit exportierte Rennen, aus einer XML-Datei, zu importieren.
	 */
	public void onCLickMenuItemImport() {

		FileChooser chooser = new FileChooser();
		chooser.setTitle("Import");
		chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".xml", "*.xml"));
		chooser.setInitialDirectory(new File(System.getProperty("user.home")));
		final File importFile = chooser.showOpenDialog(new Stage());

		new Thread(new Runnable() {

			@Override
			public void run() {
				BufferedReader input = null;
				try {
					input = new BufferedReader(new InputStreamReader(new FileInputStream(importFile)));

					String line = "";
					StringBuilder sb = new StringBuilder();
					while ((line = input.readLine()) != null) {
						sb.append(line + System.lineSeparator());
					}
					XMLReader xmlReader = XMLReaderFactory.createXMLReader();
					InputSource inputSource = new InputSource(new StringReader(sb.toString()));
					xmlReader.setContentHandler(new XMLParser());
					xmlReader.parse(inputSource);
				} catch (IOException | SAXException e) {
					// TODO evtl. parsingfehler abfangen
					e.printStackTrace();
				} finally {
					if (input != null)
						try {
							input.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
				Platform.runLater(() -> {
					fillOverviewList();
				});

			}
		}).start();

	}

	/**
	 * Diese Methode wird durch Klick auf einen Menüpunkt aufgerufen, sie bietet die Möglichkeit alle erfolgten Rennen in eine XML-Datei zu exportieren.
	 */
	public void onCLickMenuItemExport() {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Export");
		chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".xml", "*.xml"));
		chooser.setInitialDirectory(new File(System.getProperty("user.home")));
		File exportFile = new File(chooser.showSaveDialog(new Stage()).getAbsolutePath()
				+ chooser.getSelectedExtensionFilter().getDescription());
		while (exportFile.getPath().contains(".xml.xml"))
			exportFile = new File(exportFile.getPath().replaceAll(".xml.xml", ".xml"));

		Rennen[] exportdata = DBController.queryForAllRaces()
				.toArray(new Rennen[DBController.queryForAllRaces().size()]);

		String XMLdata = FiletransferHelper.raceToXMLContent(exportdata);
		try {
			FiletransferHelper.writeTextFile(exportFile, XMLdata);
			final String absolutePath = exportFile.getParentFile().getAbsolutePath();
			// Notification: Datei erfolgreich angelegt
			final JFXSnackbar notification = new JFXSnackbar(basePane);
			notification.getPopupContainer().setOnMouseClicked(e -> {
				notification.unregisterSnackbarContainer(basePane);
			});
			notification.setPrefWidth(Main.primaryStage.getWidth());
			EventHandler action = new EventHandler() {

				@Override
				public void handle(Event event) {
					try {
						Runtime.getRuntime().exec("xdg-open " + absolutePath);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					notification.unregisterSnackbarContainer(basePane);
				}
			};
			notification.show(exportFile.getName() + Main.languageBundle.getString("succesfully_created"),
					Main.languageBundle.getString("open_folder"), 3500, action);

		} catch (Exception e) {
			// Notification: Datei nicht angelegt
			JFXSnackbar notification = new JFXSnackbar(basePane);
			notification.getPopupContainer().setOnMouseClicked(f -> {
				notification.unregisterSnackbarContainer(basePane);
			});
			notification.setPrefWidth(Main.primaryStage.getWidth());
			notification.getChildren().get(0).setStyle("-fx-background-color: #C00000D0");
			notification.show(exportFile.getName() + Main.languageBundle.getString("cant_create"), 4500);

			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode wird durch Klick auf einen Menüpunkt aufgerufen, sie schließt das Programm.
	 */
	public void onCLickMenuItemClose() {
		System.exit(0);
	}

	/**
	 * Diese Methode wird durch Klick auf einen Menüpunkt aufgerufen, sie ruft die Methode showPrefGui() der Klasse Main auf.
	 */
	public void onClickMenuItemPreferences() {
		Main.showPrefGui();
	}

	/**
	 * Diese Methode wird durch Klick auf einen Menüpunkt aufgerufen, sie zeigt einen Alert mit den Programminformationen an.
	 */
	public void onClickMenuItemAbout() {
		Alert alert = new Alert(AlertType.INFORMATION, Main.languageBundle.getString("Alert_about-text2"));
		alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		alert.getDialogPane().setMaxWidth(500);
		alert.setTitle(Main.languageBundle.getString("Alert_about_title"));
		alert.setHeaderText(Main.languageBundle.getString("Alert_about-text1"));
		alert.getDialogPane().getStylesheets()
				.add(getClass().getResource("../themes/"
						+ Preferences.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme")
						+ ".css").toExternalForm());
		alert.getDialogPane().setStyle("-fx-pref-width: 500px;");

		alert.showAndWait();
	}

	/**
	 * Diese Methode ist Bestandteil der Teamverwaltung und liefert ein Team-Object auf Grundlage des ausgewählten Listeneintrages innerhalb der Teamverwaltung.
	 * 
	 * @return Team
	 */
	private Team getTeamFromTeamListSelection() {
		String teamName = tvTeamTeamView.getSelectionModel().getSelectedItem().getValue();
		return DBController.queryForSpecificTeam(teamName);
	}

	/**
	 * Diese Methode wir aufgerufen, wenn ein neues Rennen gestartet wird, sie zeigt ggf. einen hardwarehinweis an und ruft anschließend showRunningRaceStage()  der Klasse Main auf.
	 */
	private void startNewRaceScene() {
		if (Preferences.userNodeForPackage(PreferencesController.class).getBoolean("checkboxStartingDialog", true)) {
			showHardwareAlert();
			while (Main.alertStage != null && Main.alertStage.isShowing())
				;
		}

		Main.showRunningRaceStage();
		//
//		Parent root;
//		try {
//			root = FXMLLoader.load(getClass().getResource("runningRaceGui.fxml"), Main.languageBundle);
//			Main.primaryStage.setTitle("LegoRacing - Rennen läuft | " + Main.runningRace.getName());
//			Scene scene = new Scene(root, 745, 579);
//			scene.getStylesheets()
//					.add(getClass().getResource("../themes/"
//							+ Preferences.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme")
//							+ ".css").toExternalForm());
//			Main.primaryStage.setScene(scene);
//
//			Main.primaryStage.show();
//			// Hide this current window (if this is what you want)
//			// ((Node)(basePane)).getScene().getWindow().hide();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}

	/**
	 * Diese Methode zeigt einen Hinweis zur Verwendung der Hardware an.
	 */
	private void showHardwareAlert() {
		if (!Main.preferencesStage.isShowing())
			try {
				Stage newStage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("HardwareAlertGui.fxml"), Main.languageBundle);
				Scene scene = new Scene(root);
				scene.getStylesheets()
						.add(getClass().getResource("../themes/" + Preferences
								.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme") + ".css")
								.toExternalForm());
				newStage.setTitle(Main.languageBundle.getString("Alert_starting_title"));
				newStage.setScene(scene);
				newStage.setResizable(false);
				newStage.showAndWait();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

}
