package application;

import java.io.File;
import java.util.prefs.Preferences;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
/**
 * 
 * @author Robert Pfeiffer
 *
 * Dieser Controller ist mit der Viewvorlage "PreferencesGui.fxml" verknüpft.
 */
public class PreferencesController {
	@FXML
	private ComboBox<String> comboLang;
	@FXML
	private ComboBox<String> comboTheme;
	@FXML
	private ComboBox<String> comboStartRound;
	@FXML
	private ComboBox<String> comboEndRound;
	@FXML
	private ComboBox<String> comboEndRace;
	@FXML
	private CheckBox checkboxStartRound;
	@FXML
	private CheckBox checkboxEndRound;
	@FXML
	private CheckBox checkboxEndRace;
	@FXML
	private CheckBox checkboxStartingDialog;

	private Preferences prefs = Preferences.userNodeForPackage(PreferencesController.class);
	private static MediaPlayer mediaPlayer;
	private boolean needsRestart = false;

	@FXML
	private void initialize() {
		ObservableList<String> langData = FXCollections.observableArrayList();
		langData.add("Deutsch");
		langData.add("English");
		comboLang.setItems(langData);
		if (prefs.get("lang", "de").startsWith("de")) {
			comboLang.getSelectionModel().select("Deutsch");
		} else {
			comboLang.getSelectionModel().select("English");
		}

		ObservableList<String> themeData = FXCollections.observableArrayList();
		themeData.add("Light_Theme");
		themeData.add("Dark_Theme");
		comboTheme.setItems(themeData);
		comboTheme.getSelectionModel().select(prefs.get("theme", "Light_Theme"));

		checkboxStartRound.setSelected(prefs.getBoolean("checkboxStartRound", true));
		checkboxEndRound.setSelected(prefs.getBoolean("checkboxEndRound", true));
		checkboxEndRace.setSelected(prefs.getBoolean("checkboxEndRace", true));
		checkboxStartingDialog.setSelected(prefs.getBoolean("checkboxStartingDialog", true));
	}

	/**
	 * Diese Methode wird durch ein Button-klick aufgerufen, sie ruft playSound() auf.
	 */
	public void onClickBtnPlayStartRoundSound() {
		playSound("resources/startRound.wav");
	}

	/**
	 * Diese Methode wird durch ein Button-klick aufgerufen, sie ruft playSound() auf.
	 */
	public void onClickBtnPlayEndRoundSound() {
		playSound("resources/endRound.wav");
	}

	/**
	 * Diese Methode wird durch ein Button-klick aufgerufen, sie ruft playSound() auf.
	 */
	public void onClickBtnPlayEndRaceSound() {
		playSound("resources/endRace.wav");
	}

	/**
	 * Diese Methode wird durch ein Button-klick aufgerufen, sie ruft savePrefs() auf und schließt das Fenster.
	 */
	public void onClickBtnOk() {
		savePrefs();
		if (needsRestart)
			Main.resetMainGui();
		Main.preferencesStage.hide();
	}

	/**
	 * Diese Methode wird durch ein Button-klick aufgerufen, sie schließt das Fenster.
	 */	
	public void onClickBtnCancel() {
		Main.preferencesStage.hide();
	}

	/**
	 * Diese Methode wird durch ein Button-klick aufgerufen, sie ruft savePrefs() auf und lädt die geöffneten Fenster ggf. neu.
	 */
	public void onClickBtnApply() {
		savePrefs();
		if (needsRestart)
			Main.resetGui();
	}

	/**
	 * Diese Methode spielt eine Sound-Datei ab..
	 * 
	 * @param _path - Pfad der abzuspielenden Sound-Datei
	 */
	private static void playSound(String _path) {
		Media sound = new Media(new File(_path).toURI().toString());
		// Gleicher Sound -> anhalten
		if (mediaPlayer != null && mediaPlayer.getMedia().getSource().equals(sound.getSource())) {
			mediaPlayer.stop();
			mediaPlayer = null;
		}
		// Anderer Sound -> alten anhalten, neuen starten
		else {
			if (mediaPlayer != null)
				mediaPlayer.stop();
			mediaPlayer = new MediaPlayer(sound);
			mediaPlayer.play();
			mediaPlayer.setOnEndOfMedia(() -> {
				mediaPlayer.stop();
				mediaPlayer = null;
			});
		}
	}

	/**
	 * Diese Methode speichert Benutzereingaben als Preferences.
	 */
	private void savePrefs() {
		if (!comboLang.getSelectionModel().getSelectedItem().toLowerCase().startsWith(prefs.get("lang", "de"))
				|| !comboTheme.getSelectionModel().getSelectedItem().startsWith(prefs.get("theme", "Light_Theme"))) {
			needsRestart = true;
		}
		prefs.put("lang", comboLang.getSelectionModel().getSelectedItem().toLowerCase().substring(0, 2));
		prefs.put("theme", comboTheme.getSelectionModel().getSelectedItem());
		prefs.putBoolean("checkboxStartRound", checkboxStartRound.isSelected());
		prefs.putBoolean("checkboxEndRound", checkboxEndRound.isSelected());
		prefs.putBoolean("checkboxEndRace", checkboxEndRace.isSelected());
		prefs.putBoolean("checkboxStartingDialog", checkboxStartingDialog.isSelected());
	}

}
