package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import backend.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
/**
 * 
 * @author Robert Pfeiffer
 *
 * Dieser Controller ist mit der Viewvorlage "runningRaceGui.fxml" verknüpft.
 */
public class RunningRaceController {
	@FXML
	public Label lblRunningRaceTeamActually;
	@FXML
	private Label lblRunningRaceTime;
	@FXML
	private TreeView<String> tvStartingOrder;
	@FXML
	private Button btnRestartRound;
	@FXML
	private Button btnDisqualifyTeam;
	@FXML
	private Button btnRestartRace;
	@FXML
	private Button btnAbortRace;

	private boolean roundIsRunning = false;
	private TimeCounter counter;
	private MediaPlayer mediaPlayer;
	private final String startRoundPath = "resources/startRound.wav";
	private final String endRoundPath = "resources/endRound.wav";
	private final String endRacePath = "resources/endRace.wav";

	private ChangeListener<Boolean> dataChangedFlagListener = new ChangeListener<Boolean>() {
		// Eine runde wurde gestartet
		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			if (!Main.roundIsRunning) {
				if (Preferences.userNodeForPackage(PreferencesController.class).getBoolean("checkboxStartRound", true))
					playSound(startRoundPath);
				counter = new TimeCounter();
				counter.start();
				lblRunningRaceTeamActually.setText(
						Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).getTeam().getName()
								+ Main.languageBundle.getString("label_running_top_text2"));
			}
			Main.roundIsRunning = true;
			btnRestartRound.setDisable(false);
		}
	};
	private ChangeListener<Number> runningRacePositionListener = new ChangeListener<Number>() {
		// Eine Runde wurde beendet
		@Override
		public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

			if (newValue != (Number) 0 && Main.roundIsRunning) {
				if (counter != null)
					counter.abort();
				Main.runningRaceDuration.set(0);
				if (Main.arduino != null)
					Main.arduino.sendCommand(USBConnection.SEND_CODE_RESET);
				Alert alert = new Alert(AlertType.INFORMATION, Main.languageBundle.getString("Alert_running_end_round1")
						+ Main.runningRace
								.getRennLeistungen().get(Main.runningRacePosition.get() - 1).getTeam().getName()
						+ Main.languageBundle.getString("Alert_running_end_round2")
						+ Rennzeit.convertMillisToProsaString(
								Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get() - 1).getDauer())
						+ Main.languageBundle.getString("Alert_running_end_round3"));
				alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE + 30);
				alert.getDialogPane().getStylesheets()
						.add(getClass().getResource("../themes/" + Preferences
								.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme") + ".css")
								.toExternalForm());
				alert.setTitle(null);
				alert.setHeaderText(null);

				if (Main.runningRacePosition.get() == Main.runningRace.getRennLeistungen().size()) {
					alert.setContentText(alert.getContentText() + System.lineSeparator() + System.lineSeparator()
							+ Main.languageBundle.getString("Alert_running_end_race_addition"));
					alert.getDialogPane().setStyle("-fx-pref-height: 310px;");

					VBox graphicbox = new VBox(new ImageView("images/lego_minifigure.png"),
							new Label(Main.languageBundle.getString("Alert_image_source")));

					alert.setGraphic(graphicbox);
					alert.getDialogPane().setStyle("-fx-padding: 5px");
					if (Preferences.userNodeForPackage(PreferencesController.class).getBoolean("checkboxEndRace", true))
						playSound(endRacePath);
				} else if (Preferences.userNodeForPackage(PreferencesController.class).getBoolean("checkboxEndRound",
						true)) {
					playSound(endRoundPath);
				}

				alert.showAndWait();

				
			}
			if (Main.runningRacePosition.get() == Main.runningRace.getRennLeistungen().size()) {
				// Rennen ist beendet
//				Platform.runLater(() -> {
//					Main.lsu100.stopListening();
//				});
				DBController.insertRace(Main.runningRace);
				Main.runningRace = null;
				deinitListeners();
				Main.lsu100.reader.cancelRace = true;
//				try {
//					FiletransferHelper.writeTextFile(new File("hexdump.txt"), Main.dump.toString());
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (EmptyInputException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				if (counter != null)
					counter.abort();
				Main.resetMainGui();

			} else {
				Platform.runLater(() -> {
					for (TreeItem<String> item : tvStartingOrder.getRoot().getChildren()) {
						item.setExpanded(false);
					}
					tvStartingOrder.getSelectionModel().select(Main.runningRacePosition.get());
				});
				lblRunningRaceTeamActually.setText(
						Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).getTeam().getName()
								+ Main.languageBundle.getString("label_running_top_text1"));
			}
			
			Main.roundIsRunning = false;
			btnRestartRound.setDisable(true);
		}
	};
	private ChangeListener<Number> runningRaceDurationListener = new ChangeListener<Number>() {
		// Eine Runde wurde beendet
		@Override
		public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
			Platform.runLater(() -> {

				lblRunningRaceTime.setText(Rennzeit.convertMillisToString(Main.runningRaceDuration.get()));
			});
		}
	};
	private ChangeListener<TreeItem<String>> tvStartingOrderListener = new ChangeListener<TreeItem<String>>() {
		@Override
		public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue,
				TreeItem<String> newValue) {
			tvStartingOrder.getSelectionModel().select(Main.runningRacePosition.get());

		}
	};
	private ChangeListener<String> portChangeListener = new ChangeListener<String>() {
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			if (Main.lsu100Port.get().isEmpty()) {
				Platform.runLater(() -> {
					Alert alert = new Alert(AlertType.ERROR, Main.languageBundle.getString("Alert_disconnected2"));
					alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
					alert.getDialogPane().setMaxWidth(500);
					alert.setTitle(Main.languageBundle.getString("Alert_disconnected_title"));
					alert.setHeaderText(Main.languageBundle.getString("Alert_disconnected1"));
					alert.getDialogPane().getStylesheets().add(getClass().getResource("../themes/"
							+ Preferences.userNodeForPackage(PreferencesController.class).get("theme", "Light_Theme")
							+ ".css").toExternalForm());
					alert.getDialogPane().setStyle("-fx-pref-width: 500px;");

					alert.showAndWait();
				});
			} else {
				// TODO reconnected -> und nu?
			}
		}
	};

	@FXML
	public void initialize() {
		lblRunningRaceTeamActually
				.setText(Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).getTeam().getName()
						+ Main.languageBundle.getString("label_running_top_text1"));
		initListeners();
		fillTeamList();
		startUsbKram();
		Platform.runLater(() -> {
			tvStartingOrder.getSelectionModel().select(0);
			if(Main.arduino==null) {
				btnDisqualifyTeam.setVisible(false);
				btnRestartRound.setVisible(false);
			}
		});

	}

	/**
	 * Diese Methode deinitialisiert alle Listener.
	 */
	private void deinitListeners() {
		Main.dataChangedFlag.removeListener(dataChangedFlagListener);
		Main.runningRacePosition.removeListener(runningRacePositionListener);
		Main.runningRaceDuration.removeListener(runningRaceDurationListener);
		tvStartingOrder.getSelectionModel().selectedItemProperty().addListener(tvStartingOrderListener);
		Main.lsu100Port.removeListener(portChangeListener);
	}

	/**
	 * Diese Methode initialisiert alle Listener.
	 */
	private void initListeners() {
		Main.dataChangedFlag.addListener(dataChangedFlagListener);
		Main.runningRacePosition.addListener(runningRacePositionListener);
		Main.runningRaceDuration.addListener(runningRaceDurationListener);
		tvStartingOrder.getSelectionModel().selectedItemProperty().addListener(tvStartingOrderListener);
		Main.lsu100Port.addListener(portChangeListener);
	}

	/**
	 * Diese Methode befüllt die TreeView mit den Daten der teilnehmenden Teams.
	 */
	private void fillTeamList() {
		ArrayList<TreeItem<String>> treeData = new ArrayList<>();
		for (Rennleistung x : Main.runningRace.getRennLeistungen()) {

			TreeItem<String> tempTeam = new CheckBoxTreeItem<String>(x.getTeam().getName());
			ArrayList<TreeItem<String>> tempMitglieder = new ArrayList<>();
			for (String y : x.getTeam().getMitglieder()) {
				tempMitglieder.add(new TreeItem<String>(y));
			}
			tempTeam.getChildren().addAll(tempMitglieder);
			treeData.add(tempTeam);
		}
		TreeItem<String> rootItem = new TreeItem<String>("root");
		rootItem.getChildren().addAll(treeData);
		tvStartingOrder.setRoot(rootItem);
		tvStartingOrder.setShowRoot(false);
	}

	/**
	 * Diese Methode instanziiert alle USB-Verbindungen über die Klasse USBConnection.
	 */
	private void startUsbKram() {

		if (Main.lsu100 == null || Main.lsu100.in == null) {
			try {
				Main.lsu100 = new USBConnection();
				Main.lsu100.connect(Main.lsu100Port.get());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if ((Main.arduino == null || Main.arduino.out == null) && !Main.arduinoPort.get().isEmpty()) {
			try {
				Main.arduino = new USBConnection();
				Main.arduino.connect(Main.arduinoPort.get());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Main.lsu100.startListening();

	}

	/*
	 * Actionmethods
	 */

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie bricht das Rennen ab und ruft resetMainGui() der Klasse Main auf.
	 */
	public void onClickCancelRace() {
		Main.runningRace = null;
		Main.runningRacePosition.set(0);
		Main.lsu100.reader.cancelRace = true;
		if (counter != null)
			counter.abort();
		deinitListeners();
		Main.resetMainGui();
	}
	
	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie startet das aktuelle Rennen neu.
	 */
	public void onClickResetRace() {
		Main.runningRacePosition.set(0);
		Main.showRunningRaceStage();
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie startet die aktuelle Runde neu.
	 */
	public void onclickResetRound() {
		btnRestartRound.setDisable(true);
		if (counter != null)
			counter.abort();
		Main.runningRaceDuration.set(0);
		Main.roundIsRunning = false;
		if (Main.arduino != null)
			Main.arduino.sendCommand(USBConnection.SEND_CODE_RESET_AND_START_STOP);
		lblRunningRaceTeamActually
				.setText(Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).getTeam().getName()
						+ Main.languageBundle.getString("label_running_top_text1"));
	}

	/**
	 * Diese Methode wird durch einen Button-Klick aufgerufen, sie disqualifiziert das aktuelle Team, indem die Rundendauer auf 24 Stunden gesetzt wird.
	 */
	public void onClickAbortRound() {
		if (Main.roundIsRunning)
			onclickResetRound();
		Platform.runLater(() -> {
			
			if (Main.arduino != null && Main.roundIsRunning) {
				Main.arduino.sendCommand(USBConnection.SEND_CODE_START_STOP);
				Main.arduino.sendCommand(USBConnection.SEND_CODE_RESET);
			}Main.roundIsRunning = false;
			Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).setAnfangszeit(0);
			Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).setEndzeit(86399999);
			Main.runningRacePosition.set(Main.runningRacePosition.get() + 1);

			for (TreeItem<String> item : tvStartingOrder.getRoot().getChildren()) {
				item.setExpanded(false);
			}
			tvStartingOrder.getSelectionModel().select(Main.runningRacePosition.get());

			lblRunningRaceTeamActually.setText(
					Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get()).getTeam().getName()
							+ Main.languageBundle.getString("label_running_top_text1"));
		});
	}

	/**
	 * Diese Methode spielt eine Sound-Datei ab..
	 * 
	 * @param _path - Pfad der abzuspielenden Sound-Datei
	 */
	private void playSound(String _path) {
		Media sound = new Media(new File(_path).toURI().toString());
		// Gleicher Sound -> anhalten
		if (mediaPlayer != null && mediaPlayer.getMedia().getSource().equals(sound.getSource())) {
			mediaPlayer.stop();
			mediaPlayer = null;
		}
		// Anderer Sound -> alten anhalten, neuen starten
		else {
			if (mediaPlayer != null)
				mediaPlayer.stop();
			mediaPlayer = new MediaPlayer(sound);
			mediaPlayer.play();
			mediaPlayer.setOnEndOfMedia(() -> {
				mediaPlayer.stop();
				mediaPlayer = null;
			});
		}
	}

	/**
	 * 
	 * @author Robert Pfeiffer
	 *
	 * Diese Klasse ist ein Zeitzähler-Thread.
	 */
	public static class TimeCounter extends Thread {
		private boolean isRunning = true;
		private long startMillis = 0;

		/**
		 * Diese Methode bricht den Thread ab.
		 */
		public void abort() {
			this.isRunning = false;
			Platform.runLater(() -> {
				Main.runningRaceDuration.set(0);
			});
		}

		@Override
		public void run() {
			this.startMillis = System.currentTimeMillis();
			while (this.isRunning) {
				try {
					TimeUnit.MILLISECONDS.sleep(33);
					Platform.runLater(() -> {
						if (this.isRunning)
							Main.runningRaceDuration.set(System.currentTimeMillis() - this.startMillis);
					});
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
