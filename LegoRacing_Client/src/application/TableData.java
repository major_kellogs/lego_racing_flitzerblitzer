package application;

import javafx.beans.property.SimpleStringProperty;

public class TableData {
    private final SimpleStringProperty number;
    private final SimpleStringProperty name;
    private final SimpleStringProperty duration;
 
    TableData(String fName, String lName, String email) {
        this.number = new SimpleStringProperty(fName);
        this.name = new SimpleStringProperty(lName);
        this.duration = new SimpleStringProperty(email);
    }
 
    public String getNumber() {
        return number.get();
    }
    public void setNumber(String fName) {
    	number.set(fName);
    }
        
    public String getName() {
        return name.get();
    }
    public void setName(String fName) {
    	name.set(fName);
    }
    
    public String getDuration() {
        return duration.get();
    }
    public void setDuration(String fName) {
    	duration.set(fName);
    }
        
}