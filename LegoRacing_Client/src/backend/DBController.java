package backend;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import application.Main;
import javafx.application.Platform;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse händelt die Datenbank.
 */
public class DBController {

	private static final DBController dbcontroller = new DBController();
	private static Connection connection;
	private static final String DB_PATH = System.getProperty( "user.home" )+"/.local/share/HOST_LegoRacing/legoracingdb.db";

	//Treiber checken
	static {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			System.err.println("Fehler beim Laden des JDBC-Treibers");
			e.printStackTrace();
		}
	}

	private DBController() {
	}

	/**
	 * Diese Methode liefert eine instanz dieser Klasse.
	 * @return DBController
	 */
	public static DBController getInstance() {
		return dbcontroller;
	}

	/**
	 * Diese Methode stellt eine Verbindung zur Datenbank her.
	 */
	private void initDBConnection() {
		try {
			if (connection != null)
				return;
			System.out.println("Creating Connection to Database...");
			connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
			if (!connection.isClosed())
				System.out.println("... DB-Connection established");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					if (!connection.isClosed() && connection != null) {
						connection.close();
						if (connection.isClosed())
							System.out.println("Connection to Database closed");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Diese Methode initialisiert die Datenbank.
	 */
	public static void initDB() {
		boolean newDB = !(new File(DB_PATH).exists());

		DBController dbc = DBController.getInstance();
		dbc.initDBConnection();
		if (newDB) {
			try {
				Statement stmt = connection.createStatement();
				stmt.executeUpdate("CREATE TABLE rennen (name, date);");
				stmt.executeUpdate(
						"CREATE TABLE rennleistungen (rname, rdate, teamname, teammember, anfangszeit, endzeit);");
				stmt.execute("CREATE TABLE teams (name, member);");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Diese Methode liefert alle gespeicherten Rennen.
	 * 
	 * @return ArrayList<Rennen>
	 */
	public static ArrayList<Rennen> queryForAllRaces() {
		ArrayList<Rennen> tempList = new ArrayList<>();
		if (connection != null) {
			try {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT * FROM rennen ORDER BY date DESC;");
				while (rs.next()) {
					tempList.add(new Rennen(rs.getString("name"), rs.getLong("date")));
				}
				for (Rennen x : tempList) {
					rs = stmt.executeQuery("SELECT * FROM rennleistungen WHERE rdate == '" + x.getDatum()
							+ "' AND rname == '" + x.getName() + "';");
					while (rs.next()) {
						Rennleistung tempLeistung = new Rennleistung(new Team(rs.getString("teamname")));
						for (String member : rs.getString("teammember").trim()
								.split(System.getProperty("line.separator")))
							tempLeistung.getTeam().addMitglied(member);
						tempLeistung.setAnfangszeit(rs.getDouble("anfangszeit"));
						tempLeistung.setEndzeit(rs.getDouble("endzeit"));
						x.addRennLeistungen(tempLeistung);
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tempList;
	}

	/**
	 * Diese Methode liefert alle gespeicherten Teams
	 * 
	 * @return ArrayList<Team>
	 */	
	public static ArrayList<Team> queryForAllTeams() {
		ArrayList<Team> tempList = new ArrayList<>();
		if (connection != null) {
			try {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT * FROM teams;");
				while (rs.next()) {
					Team tempTeam = new Team(rs.getString("name"));
					for (String member : rs.getString("member").trim().split(System.getProperty("line.separator")))
						tempTeam.addMitglied(member);
					tempList.add(tempTeam);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tempList;
	}

	/**
	 * Diese Methode sucht in der Datenbank nach einem bestimmten Rennen-Namen und liefert ggf. das entsprechende Rennen-Object.
	 * 
	 * @param _raceName - Der Rennen-Name, nach dem gesucht werden soll
	 * @return Rennen
	 */
	public static Rennen queryForSpecificRace(String _raceName) {
		Rennen tempRennen = null;
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM rennen WHERE name =='" + _raceName + "'");
			if (rs.next()) {
				tempRennen = new Rennen(rs.getString("name"), rs.getLong("date"));
				rs = stmt.executeQuery("SELECT * FROM rennleistungen WHERE rdate == '" + tempRennen.getDatum()
						+ "' AND rname == '" + tempRennen.getName() + "';");
				while (rs.next()) {
					Rennleistung tempLeistung = new Rennleistung(new Team(rs.getString("teamname")));
					for (String member : rs.getString("teammember").trim().split(System.getProperty("line.separator")))
						tempLeistung.getTeam().addMitglied(member);
					tempLeistung.setAnfangszeit(rs.getDouble("anfangszeit"));
					tempLeistung.setEndzeit(rs.getDouble("endzeit"));
					tempRennen.addRennLeistungen(tempLeistung);
				}
			}
		} catch (SQLException e) {
			// TODO
		}
		return tempRennen;
	}
	/**
	 * Diese Methode sucht in der Datenbank nach einem bestimmten Teamnamen und liefert ggf. das entsprechende Team-Object.
	 * 
	 * @param _raceName - Der Teamname, nach dem gesucht werden soll
	 * @return Team
	 */
	public static Team queryForSpecificTeam(String _teamName) {
		Team tempTeam = null;
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM teams WHERE name =='" + _teamName + "';");
			if (rs.next()) {
				tempTeam = new Team(rs.getString("name"));
				for (String member : rs.getString("member").trim().split(System.getProperty("line.separator")))
					tempTeam.addMitglied(member);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tempTeam;
	}

	/**
	 * Diese Methode überprüft, ob ein bestimmtes Rennen in der Datenbank vorhanden ist.
	 * 
	 * @param _raceName - Der Rennen-Name, nach dem gesucht werden soll
	 * @return boolean
	 */
	public static boolean checkRaceNameExists(String _raceName) {
		return queryForSpecificRace(_raceName) != null;
	}
	
	/**
	 * Diese Methode überprüft, ob ein bestimmtes Team in der Datenbank vorhanden ist.
	 * 
	 * @param _teamName - Der Team-Name, nach dem gesucht werden soll
	 * @return boolean
	 */
	public static boolean checkTeamNameExists(String _teamName) {
		return queryForSpecificTeam(_teamName) != null;
	}

	/**
	 * Diese Methode fügt ein Rennen der Datenbank hinzu.
	 * 
	 * @param _race - Das Rennen, welches hinzugefügt werden soll
	 */
	public static void insertRace(Rennen _race) {
		Platform.runLater(()->{
			Main.progressText.set(Main.languageBundle.getString("progress_write_db"));
		});
		try {
			Statement stmt = connection.createStatement();
			if (!stmt.executeQuery("SELECT * FROM rennen WHERE date == '" + _race.getDatum() + "' AND name =='"
					+ _race.getName() + "';").next()) {
				stmt.execute("INSERT INTO rennen (name, date) VALUES ('" + _race.getName() + "', '" + _race.getDatum()
						+ "')");
				for (Rennleistung x : _race.getRennLeistungen()) {
					StringBuilder members = new StringBuilder();
					for (String member : x.getTeam().getMitglieder())
						members.append(member + System.lineSeparator());
					stmt.execute(
							"INSERT INTO rennleistungen (rname, rdate, teamname, teammember, anfangszeit, endzeit) VALUES ('"
									+ _race.getName() + "','" + _race.getDatum() + "','" + x.getTeam().getName() + "','"
									+ members.toString() + "','" + x.getAnfangszeit() + "','" + x.getEndzeit() + "')");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			Platform.runLater(()->{
				Main.progressText.set("");
			});
		}
	}

	/**
	 * Diese Methode fügt ein Team der Datenbank hinzu.
	 * 
	 * @param _team - Das Team, welches hinzugefügt werden soll
	 */
	public static void insertTeam(Team _team) throws DuplicateEntryException {
		try {
			Statement stmt = connection.createStatement();
			if (!checkTeamNameExists(_team.getName())) {
				StringBuilder sb = new StringBuilder();
				for (String member : _team.getMitglieder()) {
					sb.append(member + System.lineSeparator());
				}

				stmt.execute(
						"INSERT INTO teams (name, member) VALUES ('" + _team.getName() + "', '" + sb.toString() + "')");
			} else {
				throw new DuplicateEntryException("Dieser Teamname existiert bereits!");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode aktualisiert ein Team in der Datenbank.
	 * 
	 * @param _teamNameOld - der urpsrüngliche Name des teams
	 * @param _teamNew - der ggf. neue Name des Teams
	 * @throws DuplicateEntryException - wird geworfen, falls der neue Teamname schon vorhanden ist
	 */
	public static void updateTeam(String _teamNameOld, Team _teamNew) throws DuplicateEntryException {
		try {
			Statement stmt = connection.createStatement();

			if (!(!_teamNameOld.equals(_teamNew.getName()) && checkTeamNameExists(_teamNew.getName()))) {
				StringBuilder sb = new StringBuilder();
				for (String member : _teamNew.getMitglieder()) {
					sb.append(member + System.lineSeparator());
				}
				stmt.execute("UPDATE teams SET name ='" + _teamNew.getName() + "', member = '" + sb.toString()
						+ "' WHERE name =='" + _teamNameOld + "';");
			} else {
				throw new DuplicateEntryException("Dieser Teamname existiert bereits!");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode löscht ein Rennen aus der Datenbank.
	 * 
	 * @param _raceName - der Name des zu löschenden Rennens
	 */
	public static void deleteRace(String _raceName) {
		try {
			Statement stmt = connection.createStatement();
			stmt.execute("DELETE FROM rennleistungen WHERE rname =='" + _raceName + "';");
			stmt.execute("DELETE FROM rennen WHERE name =='" + _raceName + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode löscht ein Team aus der Datenbank.
	 * 
	 * @param _team - das Team, welches gelöscht werden soll 
	 */
	public static void deleteTeam(Team _team) {
		try {
			Statement stmt = connection.createStatement();
			stmt.execute("DELETE FROM teams WHERE name =='" + _team.getName() + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
