package backend;

/**
 * 
 * @author Robert Pfeiffer
 *
 */
public class DuplicateEntryException extends Exception {
	public DuplicateEntryException(String _watschieflief) {
		super(_watschieflief);
	}
}
