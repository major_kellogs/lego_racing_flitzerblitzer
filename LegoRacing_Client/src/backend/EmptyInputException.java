package backend;
/**
 * 
 * @author Robert Pfeiffer
 *
 */
public class EmptyInputException extends Exception {
	public EmptyInputException(String _watschieflief) {
		super(_watschieflief);
	}
}
