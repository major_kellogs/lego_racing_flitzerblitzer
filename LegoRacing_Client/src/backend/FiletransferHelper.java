package backend;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import application.Main;
import javafx.application.Platform;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse exportiert Rennen in eine CSV-, bzw. XML-Datei
 */
public class FiletransferHelper {
	
	/**
	 * Diese Methode wandelt ein Rennen in einen gültige CSV-Inhalt um.
	 * 
	 * @param exportRennen - das zu exportierende Rennen
	 * @return String
	 */
	// TODO international strings
	public static String raceToCSVContent(Rennen exportRennen) {
		StringBuilder sb = new StringBuilder("Racename:;");
		sb.append(exportRennen.getName()).append(";;").append(System.lineSeparator());
		sb.append("Datum:;").append(exportRennen.getDatumAsDate()).append(";;").append(System.lineSeparator());
		sb.append("Uhrzeit:;").append(exportRennen.getUhrzeit()).append(";;").append(System.lineSeparator());
		sb.append("Teamname;Anfangszeit(ms);Endzeit(ms);Dauer (h : m : s . ms)").append(System.lineSeparator());
		for (Rennleistung x : exportRennen.getRennLeistungen()) {
			sb.append(x.getTeam().getName() + ";").append(x.getAnfangszeit() + ";").append(x.getEndzeit() + ";")
					.append(Rennzeit.convertMillisToString(x.getDauer())).append(System.lineSeparator());
		}
		return sb.toString();
	}

	/**
	 * Diese Methode wandelt ein oder mehrere Rennen in ein gültige XML-Inhalt um.
	 * 
	 * @param rennenArray - die zu exportierenden Rennen.
	 * @return String
	 */
	public static String raceToXMLContent(Rennen... rennenArray) {
		if (rennenArray.length > 0) {
			StringBuilder sb = new StringBuilder("<data>" + System.lineSeparator());
			for (int i = 0; i < rennenArray.length; i++) {
				final int j = i;
				Platform.runLater(() -> {
					Main.progressText.set(Main.languageBundle.getString("progress_write_xml"));
				});

				sb.append("\t<race name=\"" + rennenArray[i].getName() + "\" date=\"" + rennenArray[i].getDatum()
						+ "\">" + System.lineSeparator());
				for (Rennleistung x : rennenArray[i].getRennLeistungen()) {
					sb.append("\t\t<team name=\"" + x.getTeam().getName() + "\" timeBegin=\"" + x.getAnfangszeit()
							+ "\" timeEnd=\"" + x.getEndzeit() + "\" >" + System.lineSeparator());
					for (String member : x.getTeam().getMitglieder()) {
						sb.append("\t\t\t<member name=\"" + member + "\" />" + System.lineSeparator());
					}
					sb.append("\t\t</team>" + System.lineSeparator());
				}
				sb.append("\t</race>" + System.lineSeparator());
			}
			sb.append("</data>");
			
			Platform.runLater(()->{
				Main.progressText.set("");
			});
			

			return sb.toString();
		} else
			return "";
	}

	/**
	 * Die Methode schreibt einen Text in eine Datei.
	 * 
	 * @param filePath - Der Pfad der Datei
	 * @param exportContent - Der Inhalt der Datei
	 * @throws IOException
	 * @throws EmptyInputException
	 */
	public static void writeTextFile(File filePath, String exportContent) throws IOException, EmptyInputException {
		if (exportContent.isEmpty())
			throw new EmptyInputException("naja, leer eben");
		OutputStreamWriter out;

		out = new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8");
		out.write(exportContent);
		out.flush();

		out.close();
	}
}
