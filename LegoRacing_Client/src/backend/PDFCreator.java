package backend;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse exportiert ein Rennen in eine PDF-Datei
 */
public class PDFCreator {
	private File filePath;
	private Rennen race;
	private PDDocument document;

	public PDFCreator(File _filePath, Rennen _race) throws IOException {
		this.filePath = _filePath;
		this.race = _race;
		document = new PDDocument();
		create();
	}

	/**
	 * Diese Methode erstellt und speichert ein PDF-Dokument
	 * 
	 * @throws IOException
	 */
	private void create() throws IOException {
		// Metadaten setzen
		PDDocumentInformation meta = document.getDocumentInformation();
		meta.setAuthor("HOST LegoRacing");
		meta.setTitle("LegoRacing - " + race.getName());
		meta.setCreationDate(Calendar.getInstance(new Locale("de-DE")));
		meta.setCreator("LegoRacing - FlitzerBlitzer");
		meta.setSubject("Lego Erstsemester Challenge - Siegerurkunden");

		// Rennleistungen
		ArrayList<Rennleistung> platzierungen = (ArrayList<Rennleistung>) race.getRennLeistungen().clone();
		Collections.sort(platzierungen, new Comparator<Rennleistung>() {

			@Override
			public int compare(Rennleistung o1, Rennleistung o2) {
				if (o1.getDauer() > o2.getDauer())
					return 1;
				else
					return -1;
			}
		});

		for (int i = 0; i < platzierungen.size(); i++) {
			PDPage currentPage = new PDPage(PDRectangle.A4);
			int fontSize = 45;
			PDFont font = PDType1Font.TIMES_BOLD;
			PDPageContentStream contentStream = new PDPageContentStream(document, currentPage);
			switch (i) {
			case 0:
				contentStream.drawImage(PDImageXObject.createFromFile("resources/pdfBackground1.png", document), 0, 0);
				break;
			case 1:
				contentStream.drawImage(PDImageXObject.createFromFile("resources/pdfBackground2.png", document), 0, 0);
				break;
			case 2:
				contentStream.drawImage(PDImageXObject.createFromFile("resources/pdfBackground3.png", document), 0, 0);
				break;
			default:
				contentStream.drawImage(PDImageXObject.createFromFile("resources/pdfBackground3plus.png", document), 0, 0);
			}

			//Titel des Rennens setzen
			contentStream.beginText();
			contentStream.setFont(font, fontSize);
			contentStream.setNonStrokingColor(Color.decode("#4a4a4a"));
			String text = race.getName();
			contentStream.newLineAtOffset(getOffsetCenter(currentPage, text, font, fontSize), 530);
			contentStream.showText(text);
			contentStream.endText();

			//Datum setzen
			contentStream.beginText();
			fontSize = 22;
			contentStream.setFont(font, fontSize);
			text = "Am " + race.getDatumAsDate();
			contentStream.newLineAtOffset(getOffsetCenter(currentPage, text, font, fontSize), 470);
			contentStream.showText(text);
			contentStream.endText();

			//Zwischentext 1 setzen
			contentStream.beginText();
			text = "hat das Team:";
			contentStream.newLineAtOffset(getOffsetCenter(currentPage, text, font, fontSize), 420);
			contentStream.showText(text);
			contentStream.endText();
			contentStream.beginText();
			fontSize = 30;
			contentStream.setFont(font, fontSize);
			text = platzierungen.get(i).getTeam().getName();
			contentStream.newLineAtOffset(getOffsetCenter(currentPage, text, font, fontSize), 320);
			contentStream.showText(text);
			contentStream.endText();

			//Platzierung setzen
			contentStream.beginText();
			fontSize = 40;
			contentStream.setFont(font, fontSize);
			text = "Platz " + (i + 1);
			contentStream.newLineAtOffset(getOffsetCenter(currentPage, text, font, fontSize), 220);
			contentStream.showText(text);
			contentStream.endText();

			//Zwischentext 2 setzen
			contentStream.beginText();
			fontSize = 22;
			contentStream.setFont(font, fontSize);
			text = "belegt.";
			contentStream.newLineAtOffset(getOffsetCenter(currentPage, text, font, fontSize), 170);
			contentStream.showText(text);
			contentStream.endText();

			contentStream.close();
			document.addPage(currentPage);
		}
		document.save(filePath);
		document.close();
	}

	/**
	 * Diese Methode berechnet für einen String die Position im Dokument, damit dieser zentriert dargestellt werden kann.
	 * 
	 * @param _currentPage - die Seite, auf der der String dargestellt werden soll
	 * @param _text - der String der dargestellt werden soll
	 * @param _font - die Schriftart, in der der String dargestellt werden soll
	 * @param _fontsize  - die Schriftgröße, in der der String dargestellt werden soll
	 * @return float
	 * @throws IOException
	 */
	private float getOffsetCenter(PDPage _currentPage, String _text, PDFont _font, int _fontsize) throws IOException {
		return (_currentPage.getMediaBox().getWidth() / 2 - _font.getStringWidth(_text) / 1000 * _fontsize / 2);
	}

}
