package backend;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse ist Beastandteil des Datenmodells und repräsentiert ein Rennen.
 */
public class Rennen {
	private String name;
	private ArrayList<Rennleistung> rennLeistungen;
	private Long datum;

	public Rennen(String _name) {
		this.name = _name;
		rennLeistungen = new ArrayList<Rennleistung>();
		this.datum = System.currentTimeMillis();
	}

	public Rennen(String _name, Long _date) {
		this.name = _name;
		rennLeistungen = new ArrayList<Rennleistung>();
		this.datum = _date;
	}

	/**
	 *  Diese Methode gibt den Namen des Rennens zurück.
	 *  
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 *  Diese Methode setzt den Namen des Rennens.
	 * 
	 * @param der Name des Rennens
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *  Diese Methode gibt alle Rennleistungen des Rennens zurück.
	 * 
	 * @return ArrayList<Rennleistung>
	 */
	public ArrayList<Rennleistung> getRennLeistungen() {
		return rennLeistungen;
	}

	/**
	 *  Diese Methode fügt eine Rennleistung dem Rennen hinzu.
	 * 
	 * @param die hinzuzufügende Rennleistung
	 */
	public void addRennLeistungen(Rennleistung rennLeistung) {
		this.rennLeistungen.add(rennLeistung);
	}

	public Long getDatum() {
		return this.datum;
	}

	/**
	 *  Diese Methode gibt das Datum in der Formatierung "yyyy-MM-dd" zurück.
	 * 
	 * @return String
	 */
	public String getDatumAsDate() {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(this.datum);
	}

	/**
	 * Diese Methode gibt die Uhrzeit in der Formatierung "HH:mm:ss" zurück.
	 * 
	 * @return String
	 */
	public String getUhrzeit() {
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		return formatter.format(this.datum);
	}

	/**
	 * Diese methode überprüft, ob ein Rennen formal gültig ist.
	 * 
	 * @return boolean
	 */
	public boolean isValid() {
		if (!this.getName().isEmpty() && !(this.getDatum() == 0) && this.getRennLeistungen().size() >= 2) {
			for (Rennleistung x : this.getRennLeistungen()) {
				if (x.getTeam() == null || x.getTeam().getName().isEmpty() || x.getTeam().getMitglieder().size() < 1 || x.getDauer() == 0)
					return false;
			}
			return true;
		}
		return false;
	}

}
