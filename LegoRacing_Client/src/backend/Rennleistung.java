package backend;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse ist Bestandteil des Datenmodells sie repräsentiert die Leistung eines Teams, innerhalb eines rennens.
 */
public class Rennleistung implements Comparable<Rennleistung>{
	private Team team;
	private Double anfangszeit;
	private Double endzeit;
	private double dauer;
	
	public Rennleistung(Team _team) {
		this.team = _team;
	}

	/**
	 * Diese Methode gibt die Startzeit in Millisekunden zurück.
	 * 
	 * @return Double
	 */
	public Double getAnfangszeit() {
		return anfangszeit;
	}

	/**
	 * Diese Methode setzt die Startzeit in Millisekunden.
	 * 
	 * @param - die zu setzende Startzeit
	 */
	public void setAnfangszeit(double anfangszeit) {
		this.anfangszeit = new Double( anfangszeit);
	}

	/**
	 * Diese Methode gibt die Zielzeit in Millisekunden zurück.
	 * 
	 * @return Double
	 */
	public Double getEndzeit() {
		return endzeit;
	}

	/**
	 * Diese Methode setzt die Zielzeit und die Dauer in Millisekunden.
	 * 
	 * @param - die zu setzende Zielzeit
	 */
	public void setEndzeit(double endzeit) {
		this.endzeit = new Double( endzeit);
		setDauer(endzeit-this.getAnfangszeit());
	}

	/**
	 * Dise Methode gibt das Team-Object zurück
	 * 
	 * @return Team
	 */
	public Team getTeam() {
		return team;
	}

	/**
	 * Diese Methode gibt die Dauer in Millisekunden zurück.
	 * 
	 * @return double
	 */
	public double getDauer() {
		return dauer;
	}

	/**
	 * Diese Methode setzt die Dauer in Millisekunden.
	 * 
	 * @param - die zu setzende Dauer
	 */
	private void setDauer(double dauer) {
		this.dauer = dauer;
	}

	@Override
	public int compareTo(Rennleistung o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
}
