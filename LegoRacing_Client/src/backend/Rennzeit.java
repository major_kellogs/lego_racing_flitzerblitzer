package backend;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse wandelt Rennzeiten in die darzustellenden Strings um.
 */
public class Rennzeit {

	/**
	 * Diese Methode wandelt einen Wert (Millisekunden) in einen String der Formatierung "HH:mm:ss.SSS" um.
	 * 
	 * @param - der umzuwandelnde Wert
	 * @return String
	 */
	public static String convertMillisToString(double d) {
		Date date = new Date((long) d);
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		String dateFormatted = formatter.format(date);

		return dateFormatted;
	}

	/**
	 * Diese Methode wandelt einen Wert (Millisekunden) in einen String um, der in einem Textblock gut lesbar ist.
	 * 
	 * @param - der umzuwandelnde Wert
	 * @return String
	 */
	public static String convertMillisToProsaString(double d) {
		String[] timeValues = convertMillisToString(d).split("[:|.]");
		String[] timeUnits = { " Stunde", " Minute", " Sekunde", " Millisekunde" };

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < 4; i++) {
			if (!timeValues[i].equals("00")&&!timeValues[i].equals("000")) {
				sb.append(timeValues[i] + timeUnits[i]);
				if (!timeValues[i].equals("01")&&!timeValues[i].equals("001"))
					sb.append("n");
				if(i<2)
				sb.append(", ");
				else 
					sb.append(" ");
				if (i == 2 && !timeValues[3].equals("000"))
					sb.append("und ");
			}
		}

		return sb.toString();
	}

}
