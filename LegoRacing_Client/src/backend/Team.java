package backend;

import java.util.ArrayList;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese klasse ist Bestandteil des Datenmodells und repräsentiert ein Team.
 */
public class Team {
	private String name;
	private ArrayList<String> mitglieder;

	public Team(String _name) {
		this.name=_name;
		mitglieder= new ArrayList<String>();
	}

	/**
	 * Diese Methode gibt den Namen des Team zurück.
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Diese Methode gibt eine Liste mit allen Mitgliedern zurück
	 * 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getMitglieder() {
		return mitglieder;
	}

	/**
	 * Diese Methode fügt dem Team ein Mitglied hinzu.
	 * 
	 * @param - Der Name des Mitglieds
	 */
	public Team addMitglied(String _mitglied) {
		this.mitglieder.add(_mitglied);
		return this;
	}
	
	@Override
	public String toString() {
		return this.name;
		
	}
}
