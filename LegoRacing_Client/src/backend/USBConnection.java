package backend;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import javafx.application.Platform;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.usb.UsbDevice;
import javax.usb.UsbDeviceDescriptor;
import javax.usb.UsbHub;

import application.Main;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse ist für die Serielle Kommunikation verantwortlich.
 */
public class USBConnection {
	public SerialReader reader;
	public InputStream in;
	public OutputStream out;
	
	public static byte[] SEND_CODE_RESET = {'$','d','o',':',' ','r','e','s','e','t',';'};
	public static byte[] SEND_CODE_START_STOP = {'$','d','o',':',' ','t','o','g','g','l','e',';'};
	public static byte[] SEND_CODE_RESET_AND_START_STOP = {'$','d','o',':',' ','r','e','s','t','o','g',';'};	
	
	public USBConnection() {
		super();
	}

	/**
	 * Diese Methode stellt eine Serielle Verbindung zu einem Gerät her.
	 * 
	 * @param portName - die Bezeichnung des zu nutzenden Ports
	 * 
	 * @throws NoSuchPortException
	 * @throws PortInUseException
	 * @throws UnsupportedCommOperationException
	 * @throws IOException
	 */
	public void connect(String portName)
			throws NoSuchPortException, PortInUseException, UnsupportedCommOperationException, IOException {
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
		if (portIdentifier.isCurrentlyOwned()) {
			System.out.println("Error: Port is currently in use");
		} else {

			System.out.println(portIdentifier.getPortType());

			CommPort commPort = portIdentifier.open(this.getClass().getName(), 2000);
			if (commPort instanceof SerialPort) {
				SerialPort serialPort = (SerialPort) commPort;
				serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
						SerialPort.PARITY_NONE);

				in = serialPort.getInputStream();
				out = serialPort.getOutputStream();

			} else {
				System.out.println("Error: Only serial ports are handled by this example.");
			}
		}
	}

	/**
	 * 
	 * @author Robert Pfeiffer
	 *
	 * Diese Klasse ist für das serielle Lesen von einem verbundenen Gerät zuständig, sie ist speziell auf die Datenformatierung des gerätes LSU 100 zugeschnitten. 
	 */
	public static class SerialReader extends Thread {
		InputStream in;
		public boolean cancelRace = false;

		public SerialReader(InputStream in) {
			this.in = in;
		}

		@Override
		public void run() {
			System.out.println("run thread");
			cancelRace = false;
			while (!cancelRace) {
				byte[] buffer = new byte[32]; // 18 würden wohl auch reichen, aber sicher ist sicher. :-)
				int[] unsignedDatenArray = new int[32];
				byte len = 0;
				byte chunk = 0;
				boolean complete = false;
				// der read-befehl blockiert solange bis daten kommen. daher lesen wir hier
				// immer ganze frames oder warten auf break
				while (!complete && !cancelRace) {
					try {
						chunk = (byte) this.in.read(buffer, 0, this.in.available());
						for (int i = 0; i < chunk; i++) {
							unsignedDatenArray[len + i] = buffer[i] & 0xFF;
						}
						len += chunk;
						if (len >= 4) {
							int payloadlen = (unsignedDatenArray[3] << 8) + unsignedDatenArray[2];
							if (len >= (payloadlen + 6)) {
								complete = true;
								// TODO: Prüfsumme prüfen
							}
						}
						if (chunk == 0) TimeUnit.MILLISECONDS.sleep(150);
						System.out.print(".");
					} catch (IndexOutOfBoundsException | InterruptedException | IOException e) {
						e.printStackTrace();
						len = 0;
					}
				}
				int stunde = 0;
				int minute = 0;
				int sekunde = 0;
				int millis = 0;
	
				StringBuilder sb = new StringBuilder();
				int anzahlErsaetzungen = 0;
	
				// TODO unbedingt in eine schleife verpacken! nur start stopevent bearbeiten
				if (unsignedDatenArray[4] == 1) {
					sb.append("Start bei: ");
				} else {
					sb.append("Stop bei: ");
					anzahlErsaetzungen++;
				}
				// stunde
				if (unsignedDatenArray[5 + anzahlErsaetzungen] == 16 && unsignedDatenArray[6 + anzahlErsaetzungen] == 130) {
					stunde = 2;
					anzahlErsaetzungen++;
				} else if (unsignedDatenArray[5 + anzahlErsaetzungen] == 16
						&& unsignedDatenArray[6 + anzahlErsaetzungen] == 144) {
					stunde = unsignedDatenArray[5 + anzahlErsaetzungen];
				} else {
					stunde = unsignedDatenArray[5 + anzahlErsaetzungen];
				}
				// minute
				if (unsignedDatenArray[6 + anzahlErsaetzungen] == 16 && unsignedDatenArray[7 + anzahlErsaetzungen] == 130) {
					minute = 2;
					anzahlErsaetzungen++;
				} else if (unsignedDatenArray[6 + anzahlErsaetzungen] == 16
						&& unsignedDatenArray[7 + anzahlErsaetzungen] == 144) {
					minute = unsignedDatenArray[6 + anzahlErsaetzungen];
					anzahlErsaetzungen++;
				} else {
					minute = unsignedDatenArray[6 + anzahlErsaetzungen];
				}
				// sekunde
				if (unsignedDatenArray[7 + anzahlErsaetzungen] == 16 && unsignedDatenArray[8 + anzahlErsaetzungen] == 130) {
					sekunde = 2;
					anzahlErsaetzungen++;
				} else if (unsignedDatenArray[7 + anzahlErsaetzungen] == 16
						&& unsignedDatenArray[8 + anzahlErsaetzungen] == 144) {
					sekunde = unsignedDatenArray[7 + anzahlErsaetzungen];
					anzahlErsaetzungen++;
				} else {
					sekunde = unsignedDatenArray[7 + anzahlErsaetzungen];
				}
				// millis 2 byte
				if (unsignedDatenArray[8 + anzahlErsaetzungen] == 16 && unsignedDatenArray[9 + anzahlErsaetzungen] == 130) {
					millis = 2;
					anzahlErsaetzungen++;
				} else if (unsignedDatenArray[8 + anzahlErsaetzungen] == 16
						&& unsignedDatenArray[9 + anzahlErsaetzungen] == 144) {
					millis = unsignedDatenArray[8 + anzahlErsaetzungen];
					anzahlErsaetzungen++;
				} else {
					millis = unsignedDatenArray[8 + anzahlErsaetzungen];
				}
				millis = millis << 8;
	
				if (unsignedDatenArray[9 + anzahlErsaetzungen] == 16
						&& unsignedDatenArray[10 + anzahlErsaetzungen] == 130) {
					millis += 2;
					anzahlErsaetzungen++;
				} else if (unsignedDatenArray[9 + anzahlErsaetzungen] == 16
						&& unsignedDatenArray[10 + anzahlErsaetzungen] == 144) {
					millis += unsignedDatenArray[9 + anzahlErsaetzungen];
					anzahlErsaetzungen++;
				} else {
					millis += unsignedDatenArray[9 + anzahlErsaetzungen];
				}
				String time = sb.append(stunde).append(" : ").append(minute).append(" : ").append(sekunde).append(" : ")
						.append(millis).toString();
				System.out.println(time);
				
				if (time.contains("Start")) {
					Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get())
							.setAnfangszeit(millis + sekunde * 1000 + minute * 60000 + stunde * 3600000);
					Platform.runLater(() -> {
						Main.dataChangedFlag.set(!Main.dataChangedFlag.get());
					});
				}
				if (time.contains("Stop") && Main.runningRace != null && Main.runningRace.getRennLeistungen()
						.get(Main.runningRacePosition.get()).getAnfangszeit() != null && Main.roundIsRunning) {
					Main.runningRace.getRennLeistungen().get(Main.runningRacePosition.get())
							.setEndzeit(millis + sekunde * 1000 + minute * 60000 + stunde * 3600000);
					Platform.runLater(() -> {
						Main.runningRacePosition.set(Main.runningRacePosition.get() + 1);
					});
				}
			}
			System.out.println("thread stopped");
		}

	}

	/**
	 * 
	 * @author Robert Pfeiffer
	 *
	 * Diese Klasse ist für das serielle Schreiben auf ein verbundenes Gerät zuständig, sie ist Gerättypunabhängig nutzbar.
	 */
	public static class SerialWriter implements Runnable {
		OutputStream out;
		byte[] message;
		
		public SerialWriter(OutputStream out, byte[] _message) {
			this.out = out;
			message =_message;
		}

		public void run() {
			
					try {
						this.out.write(message);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
		}
	}


	/**
	 * Diese Methode erstellt eine Instanz der Klasse SerialReader.
	 */
	public void startListening() {
		if (reader == null || !reader.isAlive()) {
			reader = new SerialReader(in);
			reader.start();
		}

	}
	
	/**
	 * Diese Klasse erstellt eine Instanz der Klasse SerialWriter und sendet darüber einen Befehl an das verbundene Gerät.
	 * 
	 * @param _command - der Befehl der gesendet wird
	 */
	public void sendCommand(byte[] _command) {
		new Thread(new SerialWriter(this.out, _command)).start();
	}

}