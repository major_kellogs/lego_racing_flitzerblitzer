package backend;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.usb4java.Context;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.HotplugCallback;
import org.usb4java.HotplugCallbackHandle;
import org.usb4java.LibUsb;
import org.usb4java.LibUsbException;

import application.Main;
import javafx.application.Platform;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse ist für das erkennen und zuweisen von verbundenen USB-Geräten verantwortlich.
 */
public class USBListener {

	EventHandlingThread thread;

	public USBListener() {
		// Initialize the libusb context
		int result = LibUsb.init(null);
		if (result != LibUsb.SUCCESS) {
			throw new LibUsbException("Unable to initialize libusb", result);
		}

		// Check if hotplug is available
		if (!LibUsb.hasCapability(LibUsb.CAP_HAS_HOTPLUG)) {
			System.err.println("libusb doesn't support hotplug on this system");
			System.exit(1);
		}

		// Start the event handling thread
		thread = new EventHandlingThread();
		thread.start();

		// Register the hotplug callback
		HotplugCallbackHandle callbackHandle = new HotplugCallbackHandle();
		result = LibUsb.hotplugRegisterCallback(null,
				LibUsb.HOTPLUG_EVENT_DEVICE_ARRIVED | LibUsb.HOTPLUG_EVENT_DEVICE_LEFT, LibUsb.HOTPLUG_ENUMERATE,
				LibUsb.HOTPLUG_MATCH_ANY, LibUsb.HOTPLUG_MATCH_ANY, LibUsb.HOTPLUG_MATCH_ANY, new Callback(), null,
				callbackHandle);
		if (result != LibUsb.SUCCESS) {
			throw new LibUsbException("Unable to register hotplug callback", result);
		}
	}

	/**
	 * Diese Methode wird aufgerufen, wenn das Programm beendet wird und stopt den EventHandlingThread.
	 */
	public void stopListening() {
		this.thread.stopListening();
	}

	/**
	 * Diese Klasse ist der LibUsb eigene Eventhandlingthread.
	 */
	static class EventHandlingThread extends Thread {
		private volatile boolean abort;

		public void stopListening() {
			this.abort = true;
		}

		/**
		 * Aborts the event handling thread.
		 */
		public void abort() {
			this.abort = true;
		}

		@Override
		public void run() {
			while (!this.abort) {
				// Let libusb handle pending events. This blocks until events
				// have been handled, a hotplug callback has been deregistered
				// or the specified time of 1 second (Specified in
				// Microseconds) has passed.
				int result = LibUsb.handleEventsTimeout(null, 1000000);
				if (result != LibUsb.SUCCESS)
					throw new LibUsbException("Unable to handle events", result);
			}
		}
	}

	/**
	 * 
	 * @author Robert Pfeiffer
	 *
	 * Diese Klasse händelt das Verbinden und Entfernen der benötigten USB-Geräte: "ELV LSU 100" und "Arduino-Nano", sie Kapselt die benötigten Daten, um diese Geräte zu erkennen. 
	 */
	static class Callback implements HotplugCallback {
		private static final int LSU_100_VENDOR_ID = 0X10c4;
		private static final int LSU_100_PRODUCT_ID = -5536;

		private static final int ARDUINO_VENDOR_ID = 0x1a86;
		private static final int ARDUINO_PRODUCT_ID = 0x7523;

		@Override
		public int processEvent(Context context, final Device device, final int event, Object userData) {
			new Thread(new Runnable() {

				@Override
				public void run() {

					DeviceDescriptor descriptor = new DeviceDescriptor();

					// /dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0

					int result = LibUsb.getDeviceDescriptor(device, descriptor);
					if (result != LibUsb.SUCCESS)
						throw new LibUsbException("Unable to read device descriptor", result);
					System.out.println("vendor: " + descriptor.idVendor());
					System.out.println("product: " + descriptor.idProduct());
					if (descriptor.idVendor() == LSU_100_VENDOR_ID && descriptor.idProduct() == LSU_100_PRODUCT_ID) {
						if (event == 1) {/// connect
								String linkpath = "/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0";
								try {
									while (new File(linkpath).getCanonicalPath().equals(linkpath)) {
									Platform.runLater(() -> {	
										Main.progressText.set(Main.languageBundle.getString("progress_portaccess"));
									});	
										TimeUnit.MILLISECONDS.sleep(250); // wait a bit for the kernel to create link
									}
									Main.progressText.set("");
									String canonpath = new File(linkpath).getCanonicalPath();
									System.out.println("is: " + canonpath);
									Main.lsu100Port.set(canonpath);
								} catch (IOException | InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println("LSU100 connected at: " + Main.lsu100Port.get());
							
						} else if (event == 2) {// disconnect
							if (!Main.lsu100Port.get().isEmpty())
								Platform.runLater(() -> {
									System.out.println("LSU100 disconnected");
									Main.lsu100Port.set("");
								});

						}
					} else if (descriptor.idVendor() == ARDUINO_VENDOR_ID
							&& descriptor.idProduct() == ARDUINO_PRODUCT_ID) {
						if (event == 1) {/// connect

							String linkpath = "/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0";
							try {
								while (new File(linkpath).getCanonicalPath().equals(linkpath)) {
									Platform.runLater(() -> {
										Main.progressText.set(Main.languageBundle.getString("progress_portaccess"));
									});

									TimeUnit.MILLISECONDS.sleep(250); // wait a bit for the kernel to create link
								}
								Main.progressText.set("");
								String canonpath = new File(linkpath).getCanonicalPath();
								System.out.println("is: " + canonpath);
								Main.arduinoPort.set(canonpath);
							} catch (IOException | InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.println("Arduino connected at: " + Main.arduinoPort.get());

						} else if (event == 2) {// disconnect
							if (!Main.arduinoPort.get().isEmpty())
								Platform.runLater(() -> {
									System.out.println("decon");
									Main.arduinoPort.set("");
								});
						}
					}

				}
			}).start();

			return 0;
		}
	}
}
