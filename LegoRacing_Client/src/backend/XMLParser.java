package backend;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import application.Main;
import javafx.application.Platform;

/**
 * 
 * @author Robert Pfeiffer
 *
 * Diese Klasse wandelt den Inhalt von XML-Dokumenten in Objekte des Typs Rennen um.
 */
public class XMLParser implements ContentHandler {
	private Rennen tempRennen;
	private Rennleistung tempLeistung;
	private StringBuilder damagedRaces = new StringBuilder();
	// private String currentValue;
	public StringBuilder currentValue = new StringBuilder();

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		currentValue.append(ch, start, length);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		
		if (localName.contains("race")) {
			tempRennen = new Rennen(atts.getValue("name"), Long.valueOf(atts.getValue("date")));
			Platform.runLater(()->{
				Main.progressText.set(Main.languageBundle.getString("progress_read"));
			});
		}

		if (localName.contains("team")) {
			tempLeistung = new Rennleistung(new Team(atts.getValue("name")));
			tempLeistung.setAnfangszeit(Double.parseDouble(atts.getValue("timeBegin")));
			tempLeistung.setEndzeit(Double.parseDouble(atts.getValue("timeEnd")));
		}

		if (localName.contains("member") && tempLeistung != null) {
			tempLeistung.getTeam().addMitglied(atts.getValue("name"));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		final String tempString = currentValue.toString().trim();
		currentValue.setLength(0);

		if (localName.equals("team") && tempLeistung != null && tempRennen != null) {
			tempRennen.addRennLeistungen(tempLeistung);
			tempLeistung = null;
		}

		if (localName.contains("race")) {
			if (tempRennen != null && tempRennen.isValid()&&!DBController.checkRaceNameExists(tempRennen.getName())) {
				DBController.insertRace(tempRennen);
			}else {
				damagedRaces.append(tempRennen.getName()+";");
			}
				tempRennen = null;
		}
	}

	@Override
	public void endDocument() throws SAXException {
		if(!damagedRaces.toString().isEmpty()) {
//			throw new DuplicateEntryException(damagedRaces.toString());
		}
		Platform.runLater(()->{
			Main.progressText.set("");
		});

	}

	@Override
	public void endPrefixMapping(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] arg0, int arg1, int arg2) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String arg0, String arg1) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {


	}

	@Override
	public void startPrefixMapping(String arg0, String arg1) throws SAXException {
		// TODO Auto-generated method stub

	}

}
