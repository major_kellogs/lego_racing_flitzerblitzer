#include "Arduino.h"

#define TOGGLEPIN 2 //grünes Kabel
#define RESETPIN 3  //lila Kabel
char commandBuffer[12];
char *bufferPointer;

void setup() {
	bufferPointer = &commandBuffer[0];

	Serial.begin(115200);
	while (!Serial)
		delay(1);

	pinMode(TOGGLEPIN, INPUT);
	pinMode(RESETPIN, INPUT);

}
/**
 * Hier werden Kommandos über die serielle Schnittstelle empfangen.
 */
void loop() {
	while (Serial.available()) {
		char c = Serial.read();
		if (c == ';') {
			*bufferPointer = '\0';
			bufferPointer = &commandBuffer[0];
			handleLSU100();
		} else {
			if (c == '$') {
				bufferPointer = &commandBuffer[0];
			}
			*bufferPointer = c;
			bufferPointer++;
		}
	}
}
/**
 * Hier wird das empfangene Kommando überprüft und ggf. ausgeführt.
 */
void handleLSU100() {
	String message = commandBuffer;
	if (message == "$do: reset") {
		//Zeitanzeige wird zurück gesetzt
		pinMode(RESETPIN, OUTPUT);
	} else if (message == "$do: toggle") {
		//Zeitmessung wird gestartet/gestopt
		pinMode(TOGGLEPIN, OUTPUT);
	} else if (message == "$do: restog") {
		//Zeitmessung wird gestartet/gestopt
		//Zeitanzeige wird zurück gesetzt
		pinMode(TOGGLEPIN, OUTPUT);
		pinMode(RESETPIN, OUTPUT);
	}

	delay(50);
	pinMode(TOGGLEPIN, INPUT);
	pinMode(RESETPIN, INPUT);
}
