var classapplication_1_1PreferencesController =
[
    [ "initialize", "classapplication_1_1PreferencesController.html#aa2a88054ab886489df33bf381eaadef1", null ],
    [ "onClickBtnApply", "classapplication_1_1PreferencesController.html#af0b6cc00c4db134cb49a8f99f87ec715", null ],
    [ "onClickBtnCancel", "classapplication_1_1PreferencesController.html#a06dbbe31edffffd62a85bf8b6243e789", null ],
    [ "onClickBtnOk", "classapplication_1_1PreferencesController.html#ad9732028e1fc890fff20fd944bb64415", null ],
    [ "onClickBtnPlayEndRaceSound", "classapplication_1_1PreferencesController.html#afefbcb33129be8f427558c9205a73357", null ],
    [ "onClickBtnPlayEndRoundSound", "classapplication_1_1PreferencesController.html#a2ebf0ab8bb9596aaae7228769181a431", null ],
    [ "onClickBtnPlayStartRoundSound", "classapplication_1_1PreferencesController.html#adedd92d5625cd6dc3f51a780564dfe01", null ],
    [ "playSound", "classapplication_1_1PreferencesController.html#afd0ae8219c4cddbb1456a33c2385db00", null ],
    [ "savePrefs", "classapplication_1_1PreferencesController.html#a6f7ed3447471e2ba6e229e53f3d49810", null ],
    [ "checkboxEndRace", "classapplication_1_1PreferencesController.html#ad2c163f65b06a4ef39d40518935c6120", null ],
    [ "checkboxEndRound", "classapplication_1_1PreferencesController.html#a3fb820b8ee2458dab5fcfef496b94910", null ],
    [ "checkboxStartingDialog", "classapplication_1_1PreferencesController.html#af7e3cb94a369354a3e6220c1c49eab42", null ],
    [ "checkboxStartRound", "classapplication_1_1PreferencesController.html#aeb49a8b3b6adf2d85bc59e678ee044f4", null ],
    [ "comboEndRace", "classapplication_1_1PreferencesController.html#acd04b4ddb7c73eda1c1a4abb92b29909", null ],
    [ "comboEndRound", "classapplication_1_1PreferencesController.html#a137c12728dc9f312d04077540c7fb293", null ],
    [ "comboLang", "classapplication_1_1PreferencesController.html#a9e6c618763a6e10577b19b4fe69ff02d", null ],
    [ "comboStartRound", "classapplication_1_1PreferencesController.html#a7224ef360294e54336e7599e6b136886", null ],
    [ "comboTheme", "classapplication_1_1PreferencesController.html#a1f6f478ecac9b500cef2561658ae221d", null ],
    [ "mediaPlayer", "classapplication_1_1PreferencesController.html#a429d7a41b163100ac0a90943cc8487a9", null ],
    [ "needsRestart", "classapplication_1_1PreferencesController.html#a3724798acd2dbe2bde77c83a46f07bef", null ],
    [ "prefs", "classapplication_1_1PreferencesController.html#a260516f43d8c06151e3d96e818dd631e", null ]
];