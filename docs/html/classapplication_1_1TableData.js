var classapplication_1_1TableData =
[
    [ "getDuration", "classapplication_1_1TableData.html#aa19feb1583e023e998adac6a2f2b47f6", null ],
    [ "getName", "classapplication_1_1TableData.html#ab0a7e4079bd3cf3ddb932b76f849726a", null ],
    [ "getNumber", "classapplication_1_1TableData.html#abe571cdec98ea5bb32b1bee97f8682f9", null ],
    [ "setDuration", "classapplication_1_1TableData.html#a75d63cf0843fa810297d1adfcf4f1ee8", null ],
    [ "setName", "classapplication_1_1TableData.html#a0d298fadf5c0277765cdaedbb77f9677", null ],
    [ "setNumber", "classapplication_1_1TableData.html#a21b5311c30f0a518c10640f60604be98", null ],
    [ "duration", "classapplication_1_1TableData.html#adfd2fc94ca24ade401e1656d68e26abb", null ],
    [ "name", "classapplication_1_1TableData.html#a4bacb40b38d19b9b954b1cf338bacf38", null ],
    [ "number", "classapplication_1_1TableData.html#ab846c21e98cbe840704683b94f25018f", null ]
];