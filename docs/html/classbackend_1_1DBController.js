var classbackend_1_1DBController =
[
    [ "DBController", "classbackend_1_1DBController.html#a60506671d04dc94616a6da701955d3f7", null ],
    [ "checkRaceNameExists", "classbackend_1_1DBController.html#a2e07e20011baf13aa0fdeeb4cfa2dd9d", null ],
    [ "checkTeamNameExists", "classbackend_1_1DBController.html#a31b39beda64eab7b262a195f40f3e8a6", null ],
    [ "deleteRace", "classbackend_1_1DBController.html#a51b4b4a29957874b89e3c56bcaa5a7a4", null ],
    [ "deleteTeam", "classbackend_1_1DBController.html#ad276cc94c158e168fd4191dae7830576", null ],
    [ "getInstance", "classbackend_1_1DBController.html#a4e0eee7d5c0a1dfa8dbbbf3a056b0aee", null ],
    [ "initDB", "classbackend_1_1DBController.html#a48467656e28f87061bebcafd47e6421b", null ],
    [ "initDBConnection", "classbackend_1_1DBController.html#a2a5b9e3546f4694268ac8e787f57b560", null ],
    [ "insertRace", "classbackend_1_1DBController.html#ad25c117b53358034276376a9b0a58c8f", null ],
    [ "insertTeam", "classbackend_1_1DBController.html#a1462e8e88674a42d3bd6d364115d271b", null ],
    [ "queryForAllRaces", "classbackend_1_1DBController.html#a16ce437df4e9dfd2199caa68f6155f5a", null ],
    [ "queryForAllTeams", "classbackend_1_1DBController.html#a27e56eb3f61da21eee76ba1a3490c247", null ],
    [ "queryForSpecificRace", "classbackend_1_1DBController.html#ae94a297b44e2ae74f723aa94c5f52878", null ],
    [ "queryForSpecificTeam", "classbackend_1_1DBController.html#aa0150468f834431423bca9a4186afd3a", null ],
    [ "updateTeam", "classbackend_1_1DBController.html#a4c2472e60108d39e557ff0f2f1536c03", null ],
    [ "connection", "classbackend_1_1DBController.html#a8bf2316bda4a0a7c0cb693c8a0f7ae9e", null ],
    [ "DB_PATH", "classbackend_1_1DBController.html#af9c55a7eccc5336ad3254fe4137044e1", null ],
    [ "dbcontroller", "classbackend_1_1DBController.html#a3367d114e42b68e9487bf52d0b8cc95c", null ]
];