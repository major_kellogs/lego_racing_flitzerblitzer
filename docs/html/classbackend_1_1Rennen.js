var classbackend_1_1Rennen =
[
    [ "Rennen", "classbackend_1_1Rennen.html#a694483484399e54c177e338591e0e913", null ],
    [ "Rennen", "classbackend_1_1Rennen.html#a16f33617c772ca261998bb66c1221364", null ],
    [ "addRennLeistungen", "classbackend_1_1Rennen.html#a9c6841a4e90a79782992cd95d00e70e5", null ],
    [ "getDatum", "classbackend_1_1Rennen.html#a129cf85d9e0a555d140126ed80fc53e6", null ],
    [ "getDatumAsDate", "classbackend_1_1Rennen.html#a31a804dee00567fb3425cda942924220", null ],
    [ "getName", "classbackend_1_1Rennen.html#a36e6f0ad2f36f185a708a2187d72f2ca", null ],
    [ "getRennLeistungen", "classbackend_1_1Rennen.html#a5126e50e20b878c09bbe49bba30b75a8", null ],
    [ "getUhrzeit", "classbackend_1_1Rennen.html#afa6e7fb59412598c3df26bf912c68aa8", null ],
    [ "isValid", "classbackend_1_1Rennen.html#a235ff9e766c1de0c74939d8ca9d7d30e", null ],
    [ "setName", "classbackend_1_1Rennen.html#a578e531a825a4bb2c2ddc8cdc0ac79d5", null ],
    [ "datum", "classbackend_1_1Rennen.html#ab286785d9019a4a8a5033c94c76897c0", null ],
    [ "name", "classbackend_1_1Rennen.html#a89605381fa46c8c13f6084158cbd00d4", null ],
    [ "rennLeistungen", "classbackend_1_1Rennen.html#a69b7ed37f154db85e7d010beeac1a51b", null ]
];