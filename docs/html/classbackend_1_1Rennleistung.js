var classbackend_1_1Rennleistung =
[
    [ "Rennleistung", "classbackend_1_1Rennleistung.html#a864fa2cfc7329b79b1f2ebd96cea0ac2", null ],
    [ "compareTo", "classbackend_1_1Rennleistung.html#a1a5aa6ccf610a3d44a2485fb70546c30", null ],
    [ "getAnfangszeit", "classbackend_1_1Rennleistung.html#ac9501027926acafa50ae8a068ab01736", null ],
    [ "getDauer", "classbackend_1_1Rennleistung.html#a7faced646317c14095aaa2e1bfcf28ac", null ],
    [ "getEndzeit", "classbackend_1_1Rennleistung.html#ab94f32932b8bfd04019dea6958c87159", null ],
    [ "getTeam", "classbackend_1_1Rennleistung.html#a422eec450d0418dffd7c2b26bad9081c", null ],
    [ "setAnfangszeit", "classbackend_1_1Rennleistung.html#ac3ec06838e1d0bd30e15b2128789f621", null ],
    [ "setDauer", "classbackend_1_1Rennleistung.html#a766dcb4158f1b5f429c070a440a6cafd", null ],
    [ "setEndzeit", "classbackend_1_1Rennleistung.html#ae95f2dfc088ca73b6fed0b3d7f7be167", null ],
    [ "anfangszeit", "classbackend_1_1Rennleistung.html#ab56e93ded3869ad311de61a1a871d960", null ],
    [ "dauer", "classbackend_1_1Rennleistung.html#aa3adb0a9feac1a23386cead9ac9d775c", null ],
    [ "endzeit", "classbackend_1_1Rennleistung.html#a733b37acb36e28440b98686c86c6d4ed", null ],
    [ "team", "classbackend_1_1Rennleistung.html#a4755e27cc85904cb1e898c1ca6c6852d", null ]
];