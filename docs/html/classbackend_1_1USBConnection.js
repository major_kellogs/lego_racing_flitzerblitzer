var classbackend_1_1USBConnection =
[
    [ "SerialReader", "classbackend_1_1USBConnection_1_1SerialReader.html", "classbackend_1_1USBConnection_1_1SerialReader" ],
    [ "SerialWriter", "classbackend_1_1USBConnection_1_1SerialWriter.html", "classbackend_1_1USBConnection_1_1SerialWriter" ],
    [ "USBConnection", "classbackend_1_1USBConnection.html#a219ebbfb1b379319923b9b748be183e4", null ],
    [ "connect", "classbackend_1_1USBConnection.html#ada0ba2d9d5efb9b82ff3919a5a3a2491", null ],
    [ "sendCommand", "classbackend_1_1USBConnection.html#ab0c7b0168e3a8b03f9cc7928ab5280e3", null ],
    [ "startListening", "classbackend_1_1USBConnection.html#af1069f84fca8d8ca7b14d35a07329696", null ],
    [ "in", "classbackend_1_1USBConnection.html#ac43a659a4731afb2d76cb2b2d3144819", null ],
    [ "out", "classbackend_1_1USBConnection.html#aa7cb37134aca644dfed1a9cbe5a3a78d", null ],
    [ "reader", "classbackend_1_1USBConnection.html#a735025a9f8f1eb5b781d35d150614588", null ],
    [ "SEND_CODE_RESET", "classbackend_1_1USBConnection.html#ac25af9e046d321741665d7b4c9aeffc1", null ],
    [ "SEND_CODE_RESET_AND_START_STOP", "classbackend_1_1USBConnection.html#a62698eb2de57cd733d2ecdf17466689e", null ],
    [ "SEND_CODE_START_STOP", "classbackend_1_1USBConnection.html#abb426c41ea4d71587ace7092ee7a2d52", null ]
];