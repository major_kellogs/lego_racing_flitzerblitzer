var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuwx",
  1: "defhmprstux",
  2: "ab",
  3: "defhmprtux",
  4: "acdefghimopqrstuw",
  5: "abcdefhilmnoprstu",
  6: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

