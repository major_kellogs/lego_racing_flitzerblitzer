var searchData=
[
  ['cancelrace',['cancelRace',['../classbackend_1_1USBConnection_1_1SerialReader.html#a27a0c7e0af2fdd14e7bc6d5781e8896a',1,'backend::USBConnection::SerialReader']]],
  ['checkbox_5falert_5fdont_5fshow_5fagain',['checkbox_alert_dont_show_again',['../classapplication_1_1HardwareAlertController.html#a5531fa8b8575ad76d8b5901851b7f99f',1,'application::HardwareAlertController']]],
  ['checkboxendrace',['checkboxEndRace',['../classapplication_1_1PreferencesController.html#ad2c163f65b06a4ef39d40518935c6120',1,'application::PreferencesController']]],
  ['checkboxendround',['checkboxEndRound',['../classapplication_1_1PreferencesController.html#a3fb820b8ee2458dab5fcfef496b94910',1,'application::PreferencesController']]],
  ['checkboxstartingdialog',['checkboxStartingDialog',['../classapplication_1_1PreferencesController.html#af7e3cb94a369354a3e6220c1c49eab42',1,'application::PreferencesController']]],
  ['checkboxstartround',['checkboxStartRound',['../classapplication_1_1PreferencesController.html#aeb49a8b3b6adf2d85bc59e678ee044f4',1,'application::PreferencesController']]],
  ['comboendrace',['comboEndRace',['../classapplication_1_1PreferencesController.html#acd04b4ddb7c73eda1c1a4abb92b29909',1,'application::PreferencesController']]],
  ['comboendround',['comboEndRound',['../classapplication_1_1PreferencesController.html#a137c12728dc9f312d04077540c7fb293',1,'application::PreferencesController']]],
  ['combolang',['comboLang',['../classapplication_1_1PreferencesController.html#a9e6c618763a6e10577b19b4fe69ff02d',1,'application::PreferencesController']]],
  ['combostartround',['comboStartRound',['../classapplication_1_1PreferencesController.html#a7224ef360294e54336e7599e6b136886',1,'application::PreferencesController']]],
  ['combotheme',['comboTheme',['../classapplication_1_1PreferencesController.html#a1f6f478ecac9b500cef2561658ae221d',1,'application::PreferencesController']]],
  ['connected',['connected',['../classapplication_1_1MainController.html#a9b7a843f1e209292e8c3d471cfb72b04',1,'application::MainController']]],
  ['connection',['connection',['../classbackend_1_1DBController.html#a8bf2316bda4a0a7c0cb693c8a0f7ae9e',1,'backend::DBController']]],
  ['counter',['counter',['../classapplication_1_1RunningRaceController.html#a19a3712408984cdeb2602f7ab46f116f',1,'application::RunningRaceController']]],
  ['currentvalue',['currentValue',['../classbackend_1_1XMLParser.html#aef74b4cd9109744a626f30b903a63685',1,'backend::XMLParser']]]
];
