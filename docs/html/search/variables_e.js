var searchData=
[
  ['send_5fcode_5freset',['SEND_CODE_RESET',['../classbackend_1_1USBConnection.html#ac25af9e046d321741665d7b4c9aeffc1',1,'backend::USBConnection']]],
  ['send_5fcode_5freset_5fand_5fstart_5fstop',['SEND_CODE_RESET_AND_START_STOP',['../classbackend_1_1USBConnection.html#a62698eb2de57cd733d2ecdf17466689e',1,'backend::USBConnection']]],
  ['send_5fcode_5fstart_5fstop',['SEND_CODE_START_STOP',['../classbackend_1_1USBConnection.html#abb426c41ea4d71587ace7092ee7a2d52',1,'backend::USBConnection']]],
  ['serialports',['serialPorts',['../classapplication_1_1Main.html#afe4c302c85b2ebda69f024ff65ca0bda',1,'application::Main']]],
  ['spraceoverview',['spRaceOverview',['../classapplication_1_1MainController.html#a85dc7ca261dcdaabc43adfd54f3360d0',1,'application::MainController']]],
  ['spteam',['spTeam',['../classapplication_1_1MainController.html#af8164e460b1ebf7026bc31509d9cc07e',1,'application::MainController']]],
  ['startmillis',['startMillis',['../classapplication_1_1RunningRaceController_1_1TimeCounter.html#a9f1fb903161bf16873ade9b4ed6b16f0',1,'application::RunningRaceController::TimeCounter']]],
  ['startroundpath',['startRoundPath',['../classapplication_1_1RunningRaceController.html#a2a290963dd04b0e8fe048a5cb12d4508',1,'application::RunningRaceController']]]
];
